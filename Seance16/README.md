# Séance 16

## Les bases de données 

https://www.oracle.com/fr/database/what-is-database/

### Les bases de données relationnelles

https://www.oracle.com/fr/database/what-is-a-relational-database/

### La modélisation de bases de données relationnelles

https://www.irit.fr/~Thierry.Millan/CNAM-NFP107/UML%20et%20les%20Bases%20de%20Donn%C3%A9es.pdf
