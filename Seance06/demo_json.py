import json


def main():
    material_stock = {
        "wood": 15,
        "metal": 10,
        "copper": 0.3,
        "paper": 2,
    }
    # Writes dict into json
    serialized_dict = json.dumps(material_stock, indent=4)    # Add indent of 4 spaces between every item of the dict + sort keys
    print(serialized_dict)
    print(type(serialized_dict))
    with open("test.json", "w") as filepath:
        json.dump(serialized_dict, filepath)
    # Read json into dict
    with open("test.json") as filepath:
        serialized_data = json.load(filepath)
    deserialized_data = json.loads(serialized_data)
    print(type(serialized_data))
    print((type(deserialized_data)))
    print(deserialized_data)

    # Write and read : more srtaightforward way
    with open("stock.json", "w") as filepath:
        json.dump(material_stock, filepath, indent=4)
    print(type(material_stock))

    with open("stock.json", "r") as filepath:
        material_stock = json.load(filepath)
    print(type(material_stock))
    print(material_stock)


if __name__ == "__main__":
    main()
