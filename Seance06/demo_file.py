def main():
    filepath = open("data.txt")   # Open the file
    print(type(filepath))
    print(filepath)
    lines = filepath.readlines()   # Stock lines of file inside a list, each line being one str element of it
    print(type(lines))
    print(f"file contains : {lines}\n")    # Another method of string formatting
    filepath.close()              # File has to be closed

    with open("data.txt", "r") as filepath:  # "r" means read. File is opened in read mode
        text = filepath.read()            # Simple read : gets file content into a single string
        print(type(text))
        print(f"file contains : {text}\n")

    with open("data.txt", "r") as filepath:
        first_line = filepath.readline()
        second_line = filepath.readline()
        print(f"line contains : {first_line}")
        print(f"second line contains : {second_line}")

    marks = {"economics": 11, "math": 14, "english": 15}
    # File open in write mode "w",
    # means write at the beginning of the document (previous content erased)
    with open("report_card.txt", "w") as filepath:
        lines = [f"{subject}: {mark}\n" for subject, mark in marks.items()]
        filepath.writelines(lines)

    mean = (marks["economics"] + marks["math"] + marks["english"]) / 3
    # File open in append mode "a",
    # meaning that things written by this code will be added at the end of file
    with open("report_card.txt", "a") as fp:
        fp.write(f"mean : {mean}\n")


if __name__ == "__main__":
    main()
