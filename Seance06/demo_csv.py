# Source : https://realpython.com/python-csv/
import csv


def main():
    with open("students.csv") as csv_filepath:
        csv_reader = csv.reader(csv_filepath, delimiter=',')
        for line_count, row in enumerate(csv_reader):
            if line_count == 0:
                print(f"Column names are {', '.join(row)}")
            else:
                print(f"{row[0]} is student in the {row[1]} specialty, and its student id is {row[2]}.")
        print(f"Processed {line_count} lines.")

    with open("students.csv") as csv_filepath:
        csv_reader = csv.DictReader(csv_filepath)
        for line_count, row in enumerate(csv_reader):
            if line_count == 0:
                print(f"Column names are {', '.join(row)}")
            print(f"{row['name']} is in the {row['specialty']} specialty, and its student id is {row['student_id']}.")
        print(f"Processed {line_count} lines.")

    with open("employee_file.csv", mode="w") as csv_filepath:
        employee_writer = csv.writer(csv_filepath, delimiter=',', quotechar='"')
        employee_writer.writerow(["Name", "Job", "Month"])
        employee_writer.writerow(["John Smith", "Accounting", "November"])
        employee_writer.writerow(["Erica Meyers", "IT", "March"])

    with open("employee_file_bis.csv", mode="w") as csv_filepath:
        fieldnames = ["emp_name", "dept", "birth_month"]
        writer = csv.DictWriter(csv_filepath, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({"emp_name": "John Smith", "dept": "Accounting", "birth_month": "November"})
        writer.writerow({"emp_name": "Erica Meyers", "dept": "IT", "birth_month": "March"})


if __name__ == "__main__":
    main()
