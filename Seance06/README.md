# Séance 6

## La gestion de fichiers

Nous allons voir dans cette partie comment lire et écrire dans un fichier.

Manipuler des fichiers en lecture et en écriture permet à la fois de travailler avec des données existantes, et de faire des sauvegardes "en dur".

En Python, il existe deux manières pour travailler avec des fichiers. Voyons la première, en mode lecture :

```python
fp = open("data.txt")  # Open the file
data = fp.readlines()  # Stock lines of file inside a variable : data will be a list, each line being one item of it
print("file contains : {}".format(data))  # Another method of string formatting
fp.close()  # File has to be closed
```

Évidemment, le fichier `data.txt` doit déjà exister dans le répertoire courant.

Voici la seconde méthode, plus "*pythonic*", à privilégier :

```python
with open("/data.txt", "r") as fp:  # "r" means read. File is opened in read mode
    data = fp.read()  # Simple read : gets file content into a single string
    print(type(data))
    print("file contains : {}".format(data))
```

Voyons maintenant comment écrire dans un fichier :

```python
marks = {"economics": 11, "math": 14, "english": 15}
# File open in write mode "w",
# means write at the beginning of the document (previous content erased)
with open("report_card.txt", "w") as filepath:
    lines = [f"{subject}: {mark}\n" for subject, mark in marks.items()]
    filepath.writelines(lines)
```

Le second argument passé à la fonction `open()` correspond au mode d'accès.

`r` signifie lecture, `w` écriture avec effacement du contenu existant avant l'écriture (au début du fichier) et `a` *append* : le curseur est placé à la fin du fichier, donc ce qui va être écrit sera placé à la suite du contenu déjà existant. Vous pouvez ajouter un `+` à la suite de la première lettre pour accéder à la fois en lecture et en écriture au fichier.

En mode écriture, si le fichier n'existe pas, il sera automatiquement créé. Lorsque vous allez lancer pour la première fois ce script, le fichier `report_card.txt` sera ainsi créé dans le répertoire.

Exemple en mode *append* :

````python
mean = (marks["economics"] + marks["math"] + marks["english"]) / 3
# File open in append mode "a",
# meaning that things written by this code will be added at the end of file
with open("report_card.txt", "a") as fp:
    fp.write(f"mean : {mean}\n")
````

### Les fichiers JSON

Travailler avec un fichier *.txt* n'est pas la solution la plus pratique lorsqu'on travaille avec des données autres que du simple texte.

Un fichier JSON est un fichier spécialement utilisé pour stocker et échanger des données. C'est l'abréviation de *JavaScript Object Notation*.

La manipulation des fichiers JSON se fait grâce au module `json`, inclus par défaut :

L'avantage d'utiliser le format JSON réside dans le fait que la conversion d'un type Python vers le format JSON se fait automatiquement.

Voici comment écrire dans un fichier JSON :

```python
import json

material_stock = {
    "wood": 15,
    "metal": 10,
    "copper": 0.3,
    "paper": 2,
}
serialized_dict = json.dumps(material_stock, indent=4)    # Add indent of 4 spaces between every item of the dict + sort keys
print(serialized_dict)
print(type(serialized_dict))
with open("test.json", "w") as filepath:
    json.dump(serialized_dict, filepath)
```

La fonction `dumps()` sert à convertir un type Python en une chaîne de caractères au format JSON. La fonction `dump()` sert à écrire cette chaîne de caractères dans un fichier *.json*.

Voici comme lire un fichier JSON :

```python
with open("test.json") as filepath:
    serialized_data = json.load(filepath)
    deserialized_data = json.loads(serialized_data)
```

La fonction `load()` permet de récupérer une chaîne de caractère à partir du fichier *.json*. La fonction `loads()` permet de convertir cette chaîne et de récupérer le dictionnaire Python.

Il n'est pas nécessaire de passer par les `str`. Utiliser `dump` et `load` suffit : 

```python
with open("stock.json", "w") as filepath:
    json.dump(material_stock, filepath)

with open("stock.json") as filepath:
    material_stock = json.load(filepath)
print(type(material_stock))
print(material_stock)
```

## Les fichiers CSV

Cf. le script `demo_csv.py` pour des examples d'accès en lecture et écriture.
