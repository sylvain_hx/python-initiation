class Trouser:
    def __init__(self, material):
        self._color = 0x000000      # Protected attribute : underscore at start of the name
        self.material = material

    def __str__(self):
        return f"The trouser is {self._color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    # Getter method for protected attribute _color
    def get_color(self):
        return self._color

    # Setter method for protected attribute _color
    def set_color(self, color):
        if not 0 <= color <= 0xFFFFFF:
            raise ValueError("The color entered is not a correct rgb value")
        self._color = color


def main():
    trouser1 = Trouser("Denim")
    trouser1._color = -2  # _color is accessible even outside the Trouser class : protection is "soft"
    trouser1.set_color(0x0011AA)  # Modify the protected attribute with the setter method
    # trouser1.set_color(-2)       # Will raise an error
    print(trouser1.get_color())  # Get the protected attribute value with the getter method


if __name__ == '__main__':
    main()
