class Vehicle:
    def __init__(self):
        self.__brand = "Generic"    # Private attribute : double underscore before the name


class Car(Vehicle):
    def __init__(self, wheels_number):
        super().__init__()
        self.wheels_number = wheels_number
        self.__brand = "Audi"       # Private attribute : different attribute from the __brand of class Vehicle


def main():
    car1 = Car(4)
    print(dir(car1))  # dir method return every member of the passed object
    car1.__brand = "Volvo"  # Create a brand-new attribute for car1 : it will not modify any private attribute of car1
    car1._Vehicle__brand = "Renaud"  # Will modify the private attribute of car1 inherited from class Vehicle
    print(dir(car1))
    print(car1._Vehicle__brand)
    print(car1._Car__brand)
    print(car1.__brand)


if __name__ == "__main__":
    main()
