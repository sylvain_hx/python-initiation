class Pawn:
    def __init__(self, x_coord, y_coord):
        self.x_coord = x_coord
        self.y_coord = y_coord
        # self.coordinates = (self.x_coord, self.y_coord)   # Use a property instead because it's "useless" as attribute

    @property
    def coordinates(self):
        return self.x_coord, self.y_coord


def main():
    pawn_1 = Pawn(1, "A")
    print(pawn_1.coordinates)
    # pawn_1.coordinates = (2, "C")     # Will raise an error because there is no setter for coordinates property


if __name__ == '__main__':
    main()
