# Séance 11

## L'encapsulation

L'encapsulation est un concept fondamental de la POO, qui désigne le fait de regrouper les données avec des méthodes qui pourront lire et manipuler ces données. Concrètement, cela signifie masquer ces données, pour en contrôler la visibilité ou non en dehors du regroupement.

Contrairement à d'autres langages, l'encapsulation n'est pas "stricte" en Python.

### Membres protégés

Un membre protégé est un membre qui n'est accessible que dans la classe où il est déclaré et dans les classes filles de celle-ci.

Les membres protégés doivent commencer par un `_`.

Il ne s'agit que d'une convention. Rien n'empêche d'accéder à ces membres depuis n'importe où, l'interpréteur Python ne l'empêchera pas. L'idée, c'est que si vous voyez un attribut ou une méthode commençant par `_`, alors cela veut dire "Vous ne devriez pas utiliser ça en dehors de la classe".

On utilise souvent les membres protégés pour... protéger un attribut. Par exemple, pour qu'un nombre qui ne peut pas être négatif le soit.

Pour pouvoir travailler sur ces attributs, on va plutôt passer par des *getters* et des *setters* :

```python
class Trouser:

    def __init__(self, material):
        self._color = 0x000000      # Protected attribute : underscore before the name
        self.material = material

    def __str__(self):
        return f"The trouser is {self._color} and is made of {self.material}. "

    # Getter method for protected attribute _color
    def get_color(self):
        return self._color

    # Setter method for protected attribute _color
    def set_color(self, color):
        if not 0 <= color <= 0xFFFFFF:
            raise ValueError("The color entered is not a correct rgb value")
        self._color = color
        

if __name__ == "__main__":
    trouser1 = Trouser("Denim")
    trouser1._color = -2  # _color is accessible even outside the Trouser class : protection is "soft"
    trouser1.set_color(0x0011AA)  # Modify the protected attribute with the setter method
    # trouser1.set_color(-2)       # Will raise an error
    print(trouser1.get_color())  # Get the protected attribute value with the getter method
```

Ici, `_color` est maintenant un attribut protégé. Pour modifier sa valeur, on appelle son *setter*. Celui-ci permet de vérifier que le code entré est bien un code RGB correct.

Mais, comme le prouve la ligne au-dessus de `t1.set_color(-2)`, l'attribut protégé peut tout à fait être modifié directement.

NB : Les méthodes peuvent aussi être protégées. Il suffit de placer un `_` devant le nom de la méthode, comme pour les attributs.

### Membres privés

Un membre privé est un membre qui n'est accessible que dans la classe où il est déclaré. Pour déclarer un membre privé, on place un double underscore `__` devant son nom.

```python
class Vehicle:

    def __init__(self):
        self.__brand = "Generic"    # Private attribute : double underscore before the name


class Car(Vehicle):

    def __init__(self, wheels_nb):
        super().__init__()
        self.wheels_nb = wheels_nb
        self.__brand = "Audi"       # Private attribute : different attribute from the __brand of class Vehicle

        
def main():
    car1 = Car(4)
    print(dir(car1))  # dir method return every member of the passed object
    car1.__brand = "Volvo"  # Create a brand-new attribute for car1 : it will not modify any private attribute of car1
    car1._Vehicle__brand = "Renaud"  # Will modify the private attribute of car1 inherited from class Vehicle
    print(dir(car1))
    print(car1._Vehicle__brand)
    print(car1._Car__brand)
    print(car1.__brand)


if __name__ == "__main__":
    main()
```

Impossible d'accéder à l'attribut `__brand` aussi bien depuis la classe `Car` que depuis le programme principal. En effet, pour protéger davantage les attributs privés, Python change le nom de la variable vu de l'extérieur : `_ClasseName__privateAttribute`.

Il faut donc faire attention : l'instruction `c1.__brand = "Volvo"` ne va pas modifier l'attribut privé `__car`, mais créer un nouvel attribut pour l'objet référencé par `c1`.

La méthode `dir(variable)` renvoie une liste de tous les membres d'un objet.

NB : il ne faut pas confondre membre privée et méthodes spéciales, qui sont les méthodes avec un double underscore avant et après leurs noms.

On comprend ainsi pourquoi l'encapsulation n'est pas "stricte" en Python. Là où, en Java par exemple, un membre privé ne pourra pas être utilisé ailleurs que dans la classe. La philosophie du langage consiste plutôt à dire que vous ne *devriez* pas utiliser ce membre, car vous risqueriez de "casser" le fonctionnement de la classe.

## Les *properties*

Un avantage du langage Python est de fournir des décorateurs pour faciliter l'encapsulation.

En effet, le décorateur `@property` qui est inclus dans Python de base, permet de définir plus "proprement" des *getters* et des *setters* :

```python
class Trouser:
    def __init__(self, color, material):
        self._color = color
        self._material = material

    def __str__(self):
        return f"Trouser is made of {self._material} and its color is {hex(self._color)}"

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, new_color):
        if not 0 <= new_color <= 0xFFFFFF:
            raise ValueError("Color must be a correct RGB value.")
        self._color = new_color

    @property
    def material(self):
        return self._material

    @material.setter
    def material(self, new_material):
        if new_material not in ("Denim", "Cotton", "Acrylics"):
            raise ValueError("Material must be either Denim, Cotton or Acrylics.")
        self._material = new_material


def main():
    black_jeans = Trouser(0, "Denim")
    print(black_jeans.color)
    black_jeans.color = 255
    print(black_jeans)
    # black_jeans.color = -34   # Will raise an error as this is calling color.setter
    black_jeans.material = "Cotton"
    print(black_jeans)
    # black_jeans.material = "Wood"   # Will raise an error as this is calling material.setter


if __name__ == '__main__':
    main()

```

L'attribut `_color` est bel et bien protégé, mais l'accès et la modification de l'attribut d'une instance de `Trouser` se fait de manière transparente, comme si celui-ci était public.


## Exercices

- Robot Simulator