class Trouser:
    def __init__(self, color, material):
        self._color = color
        self._material = material

    def __str__(self):
        return f"Trouser is made of {self._material} and its color is {hex(self._color)}"

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, new_color):
        if not 0 <= new_color <= 0xFFFFFF:
            raise ValueError("Color must be a correct RGB value.")
        self._color = new_color

    @property
    def material(self):
        return self._material

    @material.setter
    def material(self, new_material):
        if new_material not in ("Denim", "Cotton", "Acrylics"):
            raise ValueError("Material must be either Denim, Cotton or Acrylics.")
        self._material = new_material


def main():
    black_jeans = Trouser(0, "Denim")
    print(black_jeans.color)
    black_jeans.color = 255
    print(black_jeans)
    # black_jeans.color = -34   # Will raise an error as this is calling color.setter
    black_jeans.material = "Cotton"
    print(black_jeans)
    # black_jeans.material = "Wood"   # Will raise an error as this is calling material.setter


if __name__ == '__main__':
    main()
