import numpy as np

# Create and read

a = np.array([2, 4, 8, 16])
print(a)
print(type(a))

empty = np.empty(2)
zeros = np.zeros(5)
ones = np.ones(10)
print(empty)
print(zeros)
print(ones)

even_digits = np.arange(0, 10, 2)
mult_of_four = np.linspace(4, 40, 10)
print(even_digits)
print(mult_of_four)

digits = np.arange(0, 10, dtype=np.int64)
print(digits)

print(digits[1])

tu = ((10, 20, 30), (1, 2, 3), (0, -1, -2))
tab = np.array(tu)
print(tab)

print(tab[2, 1])    # If tab was a list, we should have written tab[2][1]

print(tab[tab % 2 == 0])

# Add, remove and sort
print()
a = np.array([2, 4, 6, 8, 10])
a_bis = np.append(a, [12, 14])
print(a_bis)

b = np.array([20, 30, 40, 50])
c = np.concatenate((a, b))
print(c)
print()
print(f"a = {a}\n")
a_minus = np.delete(a, [2, 4])
print(f"a_minus : {a_minus}")
a_minus = np.delete(a, slice(2, 4))
# a_minus = np.delete(a, [2, 4])
print(f"a_minus : {a_minus}")
print(f"a = \n{a}\n")

d = a[0:3:2]
print(d)

unsorted_array = np.array([4, 12, 5, 1, -5, 154])
sorted_array = np.sort(unsorted_array)
print(sorted_array)

a = np.array([[[0, 2, 4],
               [1, 3, 5]],

              [[5, 10, 15],
               [10, 20, 30]],

              [[15, 25, 20],
               [0.5, 15.5, 55]]])

print(f"dimensions : {a.ndim}, size : {a.size}, shape : {a.shape}")

# Iterate
print()
a = np.array([np.arange(0, 10, 2), np.arange(10, 20, 2), np.arange(20, 30, 2)])
print(a)
for x in a:
    print(f"x = {x}")

for x in np.nditer(a):
    print(f"nditer : x = {x}")

# Modify the shape of an array
print()
m32 = np.array([[1, 2],
                [3, 4],
                [5, 6]])
print(m32)
print()

# li = [[1, 2, 3], [4, np.nan, np.nan], [5, 6]]

m23 = m32.reshape(2, 3)
print(m23)
print()

# Other method
m23 = np.reshape(m32, newshape=(2, 3))
print(m23)

# Expand an array
print()
a = np.arange(0, 11, 2)
print(f"ndim : {a.ndim}, shape : {a.shape}")
print(a)

cv = a[:, np.newaxis]
print(f"cv ndim : {cv.ndim}, cv shape : {cv.shape}")
print(cv)

rv = a[np.newaxis, :]
print(f"rv ndim : {rv.ndim}, rv shape : {rv.shape}")
print(rv)

# stack arrays
print()
a1 = np.array([1, 2, 3, 4, 5, 6])
a2 = np.array([100, 99, 98, 97, 96, 95])

avs = np.vstack((a1, a2))
print(f"vertical stack :\n{avs}")
print()
ahs = np.hstack((a1, a2))
print(f"horizontal stack :\n{ahs}")

# split arrays
print()
avs_split = np.split(avs, 2)
print(f"split :\n{avs_split}")
print(f"first element of the object returned by split :\n{avs_split[0]}\n")
avs_split_bis = np.split(avs, 3, axis=1)
print(avs_split_bis)

# Mathematics operations
tab = np.array(((1, 2, 3), (4, 5, 6), (7, 8, 9)))
print(f"tab:\n{tab}\n")
tab2 = tab + np.ones((3, 3))
print(tab2)
tab3 = tab2 * tab
print(tab3)
tab4 = tab2 / tab3
print(tab4)
tab_broadcast = tab * 4
print(tab_broadcast)
print()

tab_sum = tab.sum()
print(tab_sum)
tab_r_sum = tab.sum(axis=1)
tab_c_sum = tab.sum(axis=0)
print(f"row sum : {tab_r_sum}, column sum : {tab_c_sum}")

# Save and load
print()
np.save("basicTable", tab)
loaded_tab = np.load("basicTable.npy")
print(loaded_tab)
