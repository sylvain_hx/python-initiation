# Séance 13

*Data Science* : science des données. Comme son nom le laisse supposer, c'est une discipline dédiée au traitement et à l'analyse de grandes quantités de données.

On "fait" de la *Data Science* dans beaucoup de domaines : sciences physiques (phénomènes météo, physique nucléaire...), informatique (*Big Data*, intelligence artificielle), économie... Peu importe ce qu'une donnée représente derrière, on travaille de toute façon toujours un peu de la même manière avec.

Python est devenu depuis quelques années un langage fortement utilisé pour faire de la *Data Science*. Notamment grâce à `numpy`, `pandas` et `matplotlib`.

De quoi s'agit-il ? De *packages*/librairies fournissant des fonctions, des objets, des variables... facilitant le traitement, l'analyse et l'affichage de données.

## NumPy

NumPy est l'acronyme de *Numeric Python*.

Pour utiliser NumPy dans son programme, il faut l'importer.

```python
import numpy as np
```

Utiliser l'alias `np` pour NumPy fait office de convention.

Alors, à quoi sert NumPy ? À créer et manipuler des *arrays*. Une *array*, ou tableau en français, est une structure de données. On peut se représenter ça comme une "grille" de données, avec une position et des manières de travailler avec ces éléments.

On pourrait tout à fait donner cette description pour une liste. Quelles sont donc les différences entre une liste et une *array* ?

Rappel : une liste est une structure homogène. Cela signifie que tous ses éléments sont du même type, là où une liste peut contenir à la fois des `int`, des `float`, des `str` ou tout autre type.

Cette contrainte en fait un objet particulièrement adapté pour des opérations mathématiques. Car comme son nom le laisse deviner, NumPy est pensé pour manipuler des nombres.

Cette "contrainte" engendre plusieurs avantages. Le premier, c'est que manipuler un *array* NumPy est beaucoup plus rapide que manipuler une liste. Le second, c'est que c'est une séquence plus compacte, c'est-à-dire qu'elle prend moins de place en mémoire.

### Créer et lire un *array*

Comment créer un *array* à partir de valeurs connues ? Via la méthode `array()` :

```python
a = np.array([1, 2, 4, 8, 16])
print(a)
```

Affichons maintenant le type de `a` (`print(type(a))`) :

La console affiche `<class 'numpy.ndarray'>`. Que signifie `nd` : *N-dimensional* ; de dimension N en français. On parle donc de tableaux multidimensionnels.

Un tableau à une dimension peut être visualisé comme une colonne, un tableau à deux dimensions comme un carré (ou une feuille d'un tableur Excel), et ainsi de suite.

NB : il est tout à fait possible de faire des listes de N dimensions, en insérant des listes dans des listes.

On peut créer rapidement un *array* "vide", rempli de 0 ou de 1 :

```python
empty = np.empty(2)
zeros = np.zeros(5)
ones = np.ones(10)
```

Vous avez normalement déjà utilisé la fonction `range()`. Il existe des méthodes servant à faire la même chose dans NumPy :

```python
even_digits = np.arange(0, 10, 2)
mult_of_four = np.linspace(4, 40, num=10)
```

Il est possible de spécifier le type de données de l'*array* :

```python
digits = np.arange(0, 10, dtype=np.int64) # integer on 64 bits
```

Comment récupérer un élément d'un *array* : Comme pour une liste

```python
print(digits[1])
```


Comment créer un *array* 2D :

```python
table = np.array(((1, 2, 3), (4, 5, 6), (7, 8, 9)))
```

ATTENTION : la méthode `array()` ne pend qu'un argument. Ici, vous voyez que les 3 tuples de 3 éléments sont elles-mêmes placées dans un tuple, qui est passé en argument de la méthode.

Comment récupérer un élément d'un *array* multidimensionnel : là encore, comme pour une liste :

```python
print(table[2, 1])
```

Il est également possible de faire de lecture conditionnelle :

```python
print(table[table % 2 == 0])
```

### Ajouter, supprimer et trier

Voici deux méthodes pour ajouter des éléments : 

```python
a = np.array([2, 4, 6, 8, 10])
a_bis = np.append(a, [12, 14])

b = np.array([20, 30, 40, 50])
c = np.concatenate((a, b))
```

Bien que l'on parle d'ajout, ces deux méthodes ne modifient pas l'*array* de départ : elles en renvoient un nouveau à partir des arguments passés en entrée.

Voici deux méthodes pour supprimer des éléments :

```python
a_minus = np.delete(a, [2, 4])

d = a[3:5]
```

Voici la méthode pour trier un *array* :

```python
unsorted_array = np.array([4, 12, 5, 1, -5, 154])
sorted_array = np.sort(unsorted_array)
```

### Les caractéristiques d'un *array*

Il est possible de connaître les dimensions, la taille et la forme d'un *array* :

```python
a = np.array([[[0, 2, 4],
               [1, 3, 5]],

              [[5, 10, 15],
              [10, 20, 30]],

              [[15, 25, 20],
              [0.5, 15.5, 55]]])

print(f"dimensions : {a.ndim}, taille : {a.size}, forme : {a.shape}")
```

### Itérer sur un *array*

On peut itérer sur un *array* avec une boucle `for` : 

```python
a = np.array([np.arange(0, 10, 2), np.arange(10, 20, 2), np.arange(20, 30, 2)])
print(a)
for x in a:
    print(f"x = {x}")
```

Sauf qu'ici, pour afficher tous les éléments de l'*array* un par un, il faudrait pour chaque ligne faire encore une fois une boucle `for`. Pour éviter cela, on peut utiliser une méthode de `ndarray`; `nditer()` :

```python
for x in np.nditer(a):
    print(f"n-dimensional iteration : x = {x}")
```

### Modifier la forme d'un *array*

Modifier la forme d'un *array* signifie obtenir un *array* possédant les mêmes données, mais ayant une forme (nombre de lignes, de colonnes) différente. Exemple :

```python
m32 = np.array([[1, 2],
                [3, 4],
                [5, 6]])

m23 = m32.reshape(2, 3)

# Other method
m23 = np.reshape(m32, newshape=(2, 3))
```

### Augmenter la dimension d'un *array*

Il est possible de créer des *arrays* de dimension N+x à partir d'un *array* de dimension N. Par exemple :

```python
a = np.arange(0, 11, 2)
print(a.shape)

cv = a[:, np.newaxis]
print(cv.shape)

rv = a[np.newaxis, :]
print(rv.shape)
```

`a` est un *array* de dimension 1. `cv`, même s'il s'agit d'un vecteur colonne, est quand même de dimension 2. Idem pour `rv` qui est un vecteur ligne.

### Créer des *arrays* à partir d'*arrays* existants

Nous avons déjà vu comment "extraire" un *array* à partir d'un autre. Mais on peut également "*stacker*" des *arrays* :

```python
avs = np.vstack((a1, a2))
print(avs)
print()
ahs = np.hstack((a1, a2))
```

Inversement, on peut "*spliter*" des *arrays* :

```python
avs_split = np.split(avs, 2)
```

### Opérations mathématiques

Il est possible d'effectuer des opérations mathématiques sur des *arrays* : 

```python
table = np.array(((1, 2, 3), (4, 5, 6), (7, 8, 9)))
table2 = table + np.ones(3, 3)

table3 = table2 * table

table4 = table2 / table3

table_broadcast = table * 4
```

Pour effectuer la somme des éléments d'un *array*, on utilise la méthode `sum()` :

```python
table_sum = table.sum()

table_r_sum = table.sum(axis=0)
table_c_sum = table.sum(axis=1)
print(f"row sum : {table_r_sum}, column sum : {table_c_sum}")
```

### Autres méthodes utiles 

- `min()`
- `max()`
- `mean()`
- `random()`
- `unique()`
- `flip()`
- `flatten()`
- `ravel()`

### Charger et sauvegarder des objets NumPy

Il est possible de sauvegarder un *array* dans un fichier `.npy`, afin de pouvoir le réutiliser plus tard. Le principe est le même qu'avec `open()` :

```python
np.save("basicTable", table)        # This instruction saves the array into a .npy file
loaded_table = np.load("basicTable.npy")    # This will load the .npy file into the array
```

### Exercice : Ranger des nombres

Créez un *array* via l'instruction suivante :

```python
a = np.random.randint(0, 1000, 100)
```

Essayez, à partir de celui-ci, de créer un *array* de forme (10, x) contenant dans chaque ligne les nombres compris dans les intervalles `[0, 100]`, `[100, 200]`... et ainsi de suite.

Astuce : il n'y aura vraisemblablement pas la même quantité de nombres pour chaque intervalle. Pour représenter une case vide, vous pouvez utiliser `np.nan` (`nan` : not a number).

## pandas

Comme pour n'importe quel module/*package*/*library*, il faut importer pandas pour l'utiliser : 

```python
import pandas as pd
```

### Le type *Series*

Le package repose sur deux types : les `series` et les `DataFrame`. Voici comment créer une `series` :

```python
s = pd.Series([0, 8, 2, 3, 55])
print(s)
```

La console va afficher en sortie :

```
0     0
1     8
2     2
3     3
4    55
dtype: int64
```

De quoi s'agit-il ? Une *series* est un *array* à 1 dimension avec des *labels* appelés *index*. Dans le *print* ci-dessus, les données de la *series* sont affichées à droite et les *index* à gauche.

Les *series* de pandas reposent sur les *arrays* de NumPy. On peut créer une *series* à partir d'un *array* :

```python
a = np.array([1, 2, 4])
s2 = pd.Series(a)
```

### Le type *DataFrame*

Abordons maintenant la *data frame*. Pour faire simple, c'est l'équivalent en Python d'un tableur Excel. Chaque ligne et colonne sont accessibles via un *label*.

Par exemple, regardons comment créer une *data frame* ayant pour *index* (*labels* des lignes) des dates :

```python
dates = pd.date_range("20211207", periods=4)
df = pd.DataFrame(np.random.randn(4, 4), index=dates, columns=list("1234"))
```

Vous pouvez voir qu'à droite de chaque ligne sont affichées les dates (qui ont été passé en *index*), et au-dessus de chaque colonne les chiffres `1 2 3 4` qui ont été passé au paramètre `columns`.

On peut également créer une *data frame* à partir d'un dictionnaire : Les clefs deviendront les noms de colonnes.

```python
d = {"fruits": [1, 2, 4, 6], "vegetables": [2, 4, 10, 15], "starches": [45, 10, 2, 4]}
df2 = pd.DataFrame(d, index=dates)
```

NB : On ne peut pas passer utiliser n'importe quel dictionnaire pour créer une *data frame*. Il faut bien sûr que pour chaque clef, la valeur soit une séquence homogène. Par contre, une colonne peut contenir un type donné (par exemple des `int`, une autre des `str`...).

### Récupérer des informations d'une *data frame*

Voici les méthodes et attributs les plus couramment utilisés :

- `df.head()` : renvoie les 2 premières lignes
- `df.tail(5)` : renvoie les 5 dernières lignes
- `df.index` : attribut contenant les *index* (noms de ligne)
- `df.columns` : attribut contenant les noms de colonne
- `df.to_numpy` : renvoie un *array* contenant les données de la *data frame*

### Extraire des données

Récupérer les données d'une colonne :

```python
df2_c1 = df2["vegetables"]
```

Récupérer les données de certaines lignes :

```python
df3_r01 = df3[0:2] # return first two lines of df3
```

Récupérer plusieurs axes :

```python
df2_select = df2.loc[[dates[0],dates[2]], ["fruits", "starches"]]
```

Ici, on affichera les données des lignes 0 et 2 pour les colonnes `fruits` et `feculents`.

Si l'on ne connaît pas les noms des labels de nos lignes et colonnes, on peut utiliser la méthode `iloc` (`i` pour *index*). La syntaxe est la même qu'avec `loc`, il faut simplement travailler avec les indices des lignes et colonnes, non pas avec les *labels* :

```python
df2_index_select = df2.iloc[[0, 2], [0, 2]] # Equivalent to previous line
```

Comme pour les *arrays*, on peut utiliser des conditions :

```python
print(df3 > 0)
print(df3.iloc[:, 2] > 0)
```

### Modifier des données

Une *Data Frame* est un type mutable. Comme pour la localisation, on peut modifier une case de notre *Frame* soit via les labels, sont via les numéros de ligne/colonne :

```python
df2.at["20211208", "starches"] = 20
df3.iat[49, 4] = 100
```

Comme pour les *arrays*, il est possible de concaténer des *data frame* : 

```python
p1 = pd.DataFrame(np.random.randn(5, 3))
p2 = pd.DataFrame(np.random.randn(5, 3))
p = pd.concat((p1, p2))
```


### NaN : *data* manquante

Dans l'exercice de tri sur NumPy, nous avons parlé de *nan* (*not a number* en anglais). 
On en retrouve également dans les *data frame*.

En effet, pour une quelconque raison, toutes les cases d'une *data frame* ne seront pas forcément remplies (capteur en panne, connexion interrompue...). Une case vide contiendra `np.nan` :

```python
df4 = df2.reindex(index=dates[0:3], columns=list(df2.columns) + ["condiments"])
```

`reindex` permet de redéfinir les index : ici suppression d'une ligne et ajout d'une colonne. `condiments` ne contient pour l'instant que des `np.nan`.

Deux méthodes peuvent être pratiques pour se débarrasser des `np.nan` :

- `df.dropna()` : renvoie la *data frame* sans les lignes contenant des NaN
- `df.fillna(value=x)` : remplace les NaN par la `value` passé en argument

### Opérations mathématiques

- `df.mean()` : pour calculer des moyennes
- `df.apply(fct)` : permet d'appliquer des fonctions mathématiques à la *data frame*. Souvent, la fonction passée en paramètre sera une fonction de NumPy.

### Lire et écrire dans un CSV

Pandas fournit des méthodes très simples d'utilisation pour lire et écrire dans un fichier csv :

```python
df2.to_csv("sales.csv")
df_csv = pd.read_csv("sales.csv")
```

## Exercices

### Stades de football

Source : https://www.kaggle.com/datasets/antimoni/football-stadiums

NB : le fichier CSV est encodé en ANSI, et non en UTF-8. Lors de la lecture du fichier avec pandas, il faut spécifier le bon encodage, via un paramètre de la fonction `read_csv` : 

```python
stadiums_df = pd.read_csv("Football Stadiums.csv", encoding="ANSI")
```

L'argument par défaut du paramètre `encoding` est `"UTF-8"`. Comme la majorité des fichiers utilisent cet encodage, nous n'avons la plupart du temps pas besoin d'expliciter l'encodage tel que ci-dessus.

1. Créez un dataframe contenant uniquement les colonnes relatives au pays et à la fédération
2. Créez un dataframe contenant uniquement les lignes relatives aux stades ayant au moins 10 000 places
3. Créez un dataframe où les stades sont rangés dans l'ordre croissant de la capacité
4. Créez un dataframe contenant uniquement les stades de l'UEFA dont le nom du pays commence par 'C'

<details>
  <summary markdown="span">Correction</summary>

```python
import pandas as pd


def main():
    df = pd.read_csv("../Seance13/Football Stadiums.csv",
                     encoding="ANSI")  # ANSI encoding not available on Linux. To verify on Mac.
    # Question 1
    df1 = df[["Confederation", "Country"]]
    print(df1)
    # Question 2
    df2 = df[df["Capacity"] >= 10000]
    print(df2)
    # Question 3
    df3 = df.sort_values("Capacity")
    print(df3[["S.No", "Stadium", "Capacity"]])  # Select only 3 columns to verify that lines are really sorted
    # Question 4
    df4 = df[(df["Confederation"] == "UEFA") & (df["Country"].str.startswith("C"))]
    print(df4)


if __name__ == "__main__":
    main()
```

</details>
