import numpy as np
import pandas as pd

s = pd.Series([0, 8, 2, 3, 55])
print(s)

a = np.array([40, 10, 25])
s2 = pd.Series(a)
print(s2)

dates = pd.date_range("20211207", periods=4)
df = pd.DataFrame(np.random.randn(4, 4), index=dates, columns=list("1234"))
print(df)

dic = {"fruits": [1, 2, 4, 6], "vegetables": [2, 4, 10, 15], "starches": [45, 10, 2, 4]}
df2 = pd.DataFrame(dic, index=dates)
print(df2)

df3 = pd.DataFrame(np.random.randn(50, 5), columns=list("ABCDE"))
# print(df3)
# print(df3.head(3))
# print(df3.tail(4))
print()
df2_c1 = df2["vegetables"]
print("Vegetables colummn :")
print(df2_c1)
print(type(df2_c1))
df3_r01 = df3[0:2]
print(df3_r01)

df3_r2 = df3.loc[2]
print("df3_r2")
print(df3_r2)

df2_r1 = df2.loc[dates[1]]
print("df2_r1")
print(df2_r1)

df2_select = df2.loc[[dates[0], dates[2]], ["fruits", "starches"]]
print(f"df2_select :\n{df2_select}\n")

df2_iselect = df2.iloc[[0, 2], [0, 2]]
print(f"df2_select with iloc : {df2_iselect}")
print()
print(df3 > 0)
print(df3.iloc[:, 2] > 0)
print(df3.loc[:, 'C'] > 0)
print()
df2.at["20211208", "starches"] = 20
df3.iat[49, 4] = 100
print(df2)
print(df3.tail(2))

p1 = pd.DataFrame(np.random.randn(5, 3))
p2 = pd.DataFrame(np.random.randn(5, 3))
p = pd.concat((p1, p2))
print(p)
value = p.iat[0, 1]
print(f"value = {value}")
df4 = df2.reindex(index=dates[0:3], columns=list(df2.columns) + ["condiments"])
print(df4)

df2.to_csv("sales.csv")
df_csv = pd.read_csv("sales.csv")
print(df_csv)
print(df_csv.sample(n=1))
