mark = 9
validated = True if mark >= 10 else False
print(f"Is course validated ? {validated}")
sentence = "only letters ?"
print("there is only letter in sentence") if sentence.isalpha() else print("There is NOT only letter in sentence")

values = (1, -12, 4, 1938.3 -123.3, 12, 89, -232, 0, 642, -729)
only_positive_values = [value for value in values if value > 0]
print(only_positive_values)
books = ["harry potter", " THE LITtle PrinCE", "the name of the rose", "to KILL a mockinGBIRd"]
titled_books = [book.title() for book in books]
print(titled_books)
