# Séance 3

## Les fonctions *built-in*

Un élément *built-in* est un élément inclus de base, sans avoir besoin d'utiliser un module/une librairie externe. Par exemple, `print()` et `input()` sont des fonctions *built-in*. Python contient beaucoup d'autres fonctions *built-in*, et certaines permettent de simplifier son code, en nous fournissant des fonctionnalités simples, qui n'ont ainsi plus besoin d'être codées "à la main".

### Les constructeurs

Le terme "constructeur" désigne la fonction qui est exécutée lors de l'instanciation d'une classe. Nous en avons déjà utilisé certains parmi la liste ci-dessous, car elles servent en général à créer des objets d'un type à partir d'un autre (par exemple créer un tuple à partir d'une liste, un entier à partir d'une chaîne de caractère...).

- `bool(obj)`
- `bytearray(obj)`
- `bytes(obj)`
- `complex(a, b)`
- `dict(obj)`
- `float(obj)`
- `frozenset(obj)`
- `int(obj)`
- `list(obj)`
- `memoryview(obj)`
- `object()`. Le type `object` est le type parent de tous les autres.
- `range(start, stop step)`
- `set(obj)`
- `slice(start, stop, step)`. Un objet de type `slice` sert à *slicer* une séquence. Les paramètres `start` et `step` sont optionnels (comme pour le type `range`). Exemple :
```python
numbers = [1, 2, 3, 4.5, 12.5, 10, 20, 34.54, 5.98]
selected_numbers = numbers[slice(2, 8, 2)]      # Selected_numbers contains 2, 4.5 and 10 (stop value is excluded)
print(selected_numbers)
```
Utiliser un `slice` est équivalent à utiliser la syntaxe `sequence[start_value:stop_value:step_value]`.
- `str(obj)`
- `tuple(obj)`

### Les fonctions opérant sur des nombres

- `abs(x)` : renvoie la valeur absolue de `x`
- `divmod(x, y)` : renvoie un tuple contenant le résultat et le reste de la division entière de `x` par `y`. Sorte de combinaison des opérateurs `//` et `%`.
- `pow(x, y)` : renvoie `x` puissance `y`. Équivalent à l'opérateur `**`.
- `round(x)` : renvoie un arrondi de `x`. Un paramètre optionnel `ndigits` permet de spécifier le nombre de chiffres après la virgule (Par défaut `0` donc arrondi à l'entier).

### Les fonctions de conversion numérique

- `bin(x)` : renvoie une chaine de caractères correspondant à la représentation binaire de `x`. Par exemple, si `x` vaut 4, la fonction renverra `"0b100"`.
- `hex(x)` : renvoie une chaine de caractères correspondant à la représentation hexadécimale de `x`. Par exemple, si `x` vaut 12, la fonction renverra `"0xC"`.
- `oct(x)` : renvoie une chaine de caractères correspondant à la représentation octale de `x`. Par exemple, si `x` vaut 9, la fonction renverra `"0o10"`.

### Les fonctions de conversion de caractères

- `ascii(obj)` : renvoie une chaîne de caractères "lisible" de `obj` (peu importe son type). Si `obj` est un `str` ne contenant pas uniquement des caractères ASCII, ceux-ci sont remplacés par un caractère d'échappement.
- `chr(unicode_value)` : renvoie une chaîne de caractères contenant le caractère correspondant à la valeur de `unicode_value`.
- `format(value, form)` : renvoie une chaîne de caractères représentant `value` au format spécifié par le paramètre `form`.
- `ord(char)` : renvoie le code unicode du caractère `char`.

### Les fonctions opérant sur des itérables

Un itérable est un objet sur lequel on peut itérer. Autrement dit, c'est un objet qui peut être parcouru par une boucle `for`. Les collections (et donc les séquences) sont des itérables.

- `all(iterable)` : renvoie `True` si tous les éléments d'`iterable` sont `True`.
- `any(iterable)` : renvoie `True` si au moins un des éléments d'`iterable` est `True`.
- `enumerate(iterable)` : utilisée dans une boucle `for`, renvoie à chaque itération un tuple contenant l'indice et l'élément de l'itérable. Un paramètre optionnel permet de définir la valeur de départ de l'indice (par défaut `0`).
- `filter(function, iterable)` : renvoie un itérable (précisément un objet de type `filter`) contenant uniquement les éléments d'`iterable` pour lesquels `function(iterable)` renvoie `True`.
- `len(iterable)` : renvoie un entier égal au nombre d'éléments présents dans `iterable`.
- `map(function, iterable)` : applique la fonction `function` sur chaque élément de `iterable` et renvoie le tout dans un itérable (précisément un objet de type `map`).
- `max(iterable)` : renvoie la plus grande valeur d'`iterable`.
- `min(iterable)` : renvoie la plus petite valeur d'`iterable`.
- `reversed(iterable)` : renvoie un itérateur (précisément un objet de type `reversed`), qui, utilisée avec une boucle `for`, renverra les éléments d'`iterable` dans l'ordre inverse.
- `sorted(iterable)` : renvoie une version triée d'`iterable`. *NB* : l'objet renvoyé est toujours de type `•list`, même si `iterable` n'en est pas une (si c'est un tuple ou un set par exemple).
- `sum(iterable)` : renvoie la somme de tous les éléments d'`iterable`.
- `zip(iterable_one, iterable_two,...)` : utilisée dans une boucle `for`, renvoie à chaque itération un tuple contenant les éléments d'indice x de `iterable_one`, `iterable_two` (et plus si plus de deux itérables sont en entrée).

### Autres fonctions utiles

- `id(obj)` : renvoie l'identifiant (un `int`) de `obj`. Chaque objet a un identifiant unique.
- `isinstance(obj, cls)` : renvoie `True` si `obj` est une instance de la classe `cls` (dit autrement : renvoie `True` si obj est de type `cls`).
- `type(obj)` : renvoie la classe dont `obj` est une instance.
- `help(obj)` : utilisé dans le *REPL*. Affiche la documentation de `obj` (donc très utile si `obj` est une classe).

## Les méthodes d'instance de `str`

La classe `str` contient beaucoup de méthodes d'instance. Une méthode d'instance est une fonction définie dans uen classe, qui est appelée "sur une instance de cette classe". Pour appeler une méthode d'instance, on écrit `variable.instance_method()`. Exemple :

```python
sentence = "hello world"    # sentence references to a str object
sentence.upper()    # upper() is an instance method of class str
```

Ci-dessous, la variable `s` référence vers un `str`. 

### Les méthodes vérifiant des conditions

Les méthodes ci-dessous renvoient `True` si l'instance sur laquelle elles sont appelées respecte une condition particulière :

- `s.endswith(string)` : si la chaîne se termine par `string`.
- `s.isalnum()` : si la chaîne ne contient que des caractères alphanumériques (c.-à-d. des lettres et des caractères numériques, cf. méthodes `isalpha()` et  `isnumeric()`).
- `s.isalpha()` : si la chaîne ne contient que des lettres.
- `s.isascii()` : si la chaine ne contient que des caractères ASCII.
- `s.isdecimal()` : si la chaîne ne contient que des caractères décimaux (c.-à-d. les chiffres de 0 à 9).
- `s.isdigit()` : si la chaine ne contient que des *digits (ce qui inclut les 10 chiffres, mais pas seulement). Par exemple les exposants comme ² sont inclus). **Attention** : le `-` et le `.` ne sont pas considérés comme des *digits*.
- `s.isidentifier()` : si la chaine est un identifiant valide. Un identifiant valide ne peut contenir que des chiffres, des lettres (sans accent, cédille...) et des *underscores*.
- `s.islower()` : si toutes les lettres de la chaîne sont des minuscules.
- `s.isnumeric()` : si la chaine ne contient que des caractères numériques. Par exemple les fractions (`¾`) sont des caractères numériques. C'est un regroupement plus large que les *digits*.
- `s.isprintable()` : si la chaine est affichable (Rappel : un caractère n'est pas forcément affichable, cf. le code ASCII).
- `s.isspace()` : si la chaine ne contient que des caractères "blancs" (un espace, une tabulation, un saut à la ligne...).
- `s.istitle()` : si la chaîne est un titre : c.-à-d. si tous les mots commencent par une majuscule.
- `s.isupper()` : si toutes les lettres de la chaîne sont des majuscules.
- `s.startswith(string)` : si la chaîne commence par `string`.

### Les méthodes renvoyant une nouvelle chaîne

**ATTENTION** : il faut se rappeler qu'un objet de type `str` est immuable : toutes ces méthodes renvoient une nouvelle chaîne de caractère, sans modifier l'original.

- `s.capitalize()` : renvoie la chaîne avec le premier caractère en majuscule si celui-ci était une lettre en minuscules.
- `s.casefold()` : renvoie la chaîne de caractères en minuscules. Similaire à `lower()`, mais plus "agressif".
- `s.center(length)` : renvoie la chaîne centrée. Des espaces avant et après la chaîne sont ajoutées pour avoir une longueur égale à `length`.
- `s.encode(encoding)` : renvoie la chaîne encodée à l'encodage souhaitée. Par défaut `"utf-8"`.
- `s.expandtabs(length)` : renvoie la chaine de caractère avec des tabulations dont la longueur est égale à `length`. Par défaut `8`.
- `s.join(iterable)` : renvoie une chaîne qui met bout à bout les éléments d'`iterable`, chacun séparés par la chaîne sur laquelle est appelée la méthode. Exemple :
```python
message = " ".join(["these", "are", "words", "from", "a", "sentence"])
print(message)
```
- `s.ljust(length)` : renvoie la chaine alignée à gauche, de longueur `length`, en remplissant éventuellement avec le caractère `char`.
- `s.lower()` : renvoie la chaîne en minuscules.
- `s.lstrip()` : renvoie ula chaîne sans les éventuels *whitespaces* placés à gauche.
- `s.replace(str_to_be_replaced, replacement_str)` : renvoie la chaîne ou les occurrences de `str_to_be_replaced` ont été remplacées parr `replacement_str`.
- `s.rjust(length, char)` : renvoie une chaine alignée à droite, de longueur `length`, en remplissant éventuellement avec le caractère `char`.
- `s.rstrip()` : renvoie la chaîne sans les éventuels *whitespaces* placés à droite.
- `s.strip()` : renvoie la chaîne sans les éventuels *whitespaces* placés à gauche et à droite.
- `s.swapcase()` : renvoie la chaîne où les minuscules sont devenues majuscules et inversement.
- `s.title()` : renvoie la chaîne au format titre : chaque mot commence par une majuscule.
- `s.translate(mapping_table)` : renvoie la chaîne de caractères "traduite" à partir de `mapping_table`. Cette table est générée grâce à la fonction `maketrans()`.
- `s.upper()` : renvoie la chaîne en majuscules.
- `s.zfill(length)` : renvoie la chaîne de longueur `length`, où des 0 ont été ajoutés au début de celle-ci.

### Autres méthodes

- `s.count(chars)` : renvoie le nombre d'occurrences de `chars`.
- `s.find(chars)` : renvoie l'indice de la première occurrence de `chars` si elle est présente, `-1` sinon.
- `s.format()` : renvoie la chaîne formatée. Relativement obsolète depuis l'implémentation des *fstrings*.
- `s.index(chars)` : renvoie l'indice de la première occurrence de `chars` si elle est présente, lève une exception sinon.
- `s.maketrans(x, y)` : à utiliser conjointement avec `translate()`. Renvoie une *mapping table* de traduction. Le caractère d'indice i de `x` sera remplacé par le caractère d'indice i de `y` lors de l'appel de `translate(mapping_table)`.
- `s.rfind(chars)` : renvoie l'indice de la dernière occurrence de `chars` si elle est présente, `-1` sinon.
- `s.rindex()` : renvoie l'indice de la dernière occurrence de `chars` si elle est présente, lève une exception sinon.
- `s.rpartition(value)` : renvoie un tuple de 3 éléments. Le premier : ce qui est à gauche de `value`. Le deuxième : `value`. Le troisième : ce qui est à droite de `value`.
- `s.rsplit(separator, max)` : renvoie les morceaux de la chaîne "découpée" sous forme de liste, en commençant par la droite. `separator` étant le caractère qui délimite où découper. `max` définir le nombre maximal de découpages à faire. Par défaut, sa valeur est `-1`, et le résultat renvoyé par la fonction sera le même que celui de la fonction `split()`.
- `s.split(separator,  max)` : renvoie les morceaux de la chaîne "découpée" sous forme de liste, en commençant par la gauche. `separator` étant le caractère qui délimite où découper. `max` définir le nombre maximal de découpages à faire.
- `s.splitlines()` : renvoie les morceaux de la chaîne "découpée" sous forme de liste. Le découpage se fait en fonction du caractère saut de ligne `\n`.

## Les méthodes d'instance de `list` et `tuple`

Ci-dessous, la variable `l` référence vers une `list`. 

### Les méthodes pour ajouter des éléments

- `l.append(obj)` : ajoute `obj` à la fin de la liste.
- `l.extend(iterable)` : ajoute les éléments d'`iterable` à la fin de la liste.
- `l.insert(index, obj)` : insère `obj` dans la liste, à l'indice `index`.

#### Les méthodes pour retirer des éléments

- `l.clear()` : retire tous les éléments de la liste.
- `l.pop(index)` : retire l'élément d'indice `index`.
- `l.remove(obj)` : retire la première occurrence de `obj`.

### Les méthodes pour modifier la liste

- `l.reverse()` : inverse les éléments de la liste.
- `l.sort()` : trie la liste.

### Autres méthodes

- `l.copy()` : renvoie une copie de la liste.
- `l.count(obj)` : renvoie le nombre d'occurrences d'`obj` dans la liste. **Aussi une méthode de *tuple***
- `l.index(obj)` : renvoie l'indice de la première occurrence d'`obj`. **Aussi une méthode de *tuple***

## Deux *one-liners* utiles

On appelle *one-liner* une instruction qui "fait beaucoup de choses" en une seule ligne.

### L'opérateur ternaire `if else`

Le *one-liner* `if else`, aussi appelée opérateur ternaire, est une instruction permettant d'écrire une structure conditionnelle avec un `if` et un `else en une ligne. Exemples :

```python
mark = 14
validated = True if mark >= 10 else False
print(f"Is course validated ? {validated}")
sentence = "only letters ?"
print("there is only letter in sentence") if sentence.isalpha() else print("There is NOT only letter in sentence")
```

### Les *list comprehensions*

Une *list comprehension** est une instruction permettant de créer une liste à partir d'une autre, via une boucle `for` et sous une condition éventuelle, le tout en une seule ligne. Exemples :

```python
books = ["harry potter", " THE little PrinCE", "the name of the rose", "to KILL a mockingBIRd"]
capitalized_books = [book.capitalize() for book in books]   # map() equivalent
print(capitalized_books)
values = (1, -12, 4, 1938.3 -123.3, 12, 89, -232, 0, 642, -729)
only_positive_values = [value for value in values if value > 0]     # filter() equivalent
print(only_positive_values)
```

## Exercices

- Little Sister's Essay
- Chaitana's Colossal Coaster
- Making the Grade
- Tisbury Treasure Hunt
- Bob
- Reverse String
- Pangram
- Isogram
- ISBN Verifier
- Rotational Cipher
- RNA Transcription
- Wordy
- Resistor Color
- Resistor Color Duo
- Resistor Color Trio
- Secret Handshake
- Anagram
- House
- Binary Search
- Hamming
- Flatten Array
- Difference of Squares
- List Ops
- Sum of Multiples
- Pig Latin
- Sublist
- Prime Factors
- Acronym
- Series
- Run-Length Encoding
- Roman Numerals
