# Séance 9

## L'héritage

L'héritage désigne la possibilité de créer une classe qui va dériver, ou bien étendre les propriétés d'une classe. On parle ainsi de classe mère/parent et de classe fille/enfant.

```python
class Vehicle:

    def __init__(self, brand):
        self.brand = brand
    
    def __str__(self):
        return f"The vehicle is a {self.brand}"

class Car(Vehicle):

    def __init__(self, brand, wheels_nb):
        super().__init__(brand)
        self.wheels_nb = wheels_nb

    def __str__(self):
        return f"The car is a {self.brand} and has {self.wheels_nb} wheels"
```

Pour qu'une classe hérite d'une autre, il suffit d'ajouter le nom de la classe mère entre parenthèses après le nom de la classe fille.

NB : Lorsqu'on crée une classe qui "n'hérite de rien", elle hérite en fait de la classe `object`, classe mère de toutes les autres en Python.

Créons un bloc *main* pour afficher le `print` d'une instance de chaque classe :

```python
def main():
    v = Vehicle("Suzuki")
    c = Car("Ford", 4)

    print(v)
    print(c)
    
    
if __name__ == "__main__":
    main()
```

Et la sortie console :

```
The vehicle is a Suzuki
The car is a Ford and has 4 wheels
```

La commande `print(v)`, appelle bien la méthode `__str__()` de la classe `Vehicle`, et `print(c)` la méthode du même nom, mais de la classe `Car`.

On appelle cela une **redéfinition** (on utilise souvent le terme anglais *override*) de méthode. Cela permet de redéfinir un comportement d'une méthode d'une classe mère dans une classe fille.

La première ligne du constructeur de `Car` fait appel à la méthode `super()`. Cette méthode permet d'accéder aux membres de classe mère directement, sans passer par son nom. La ligne en question va donc en fait appeler le constructeur de la classe `Vehicle`.

Ajoutons maintenant un attribut et deux méthodes dans la classe `Vehicle` :

```python
class Vehicle:

    def __init__(self, brand):
        self.brand = brand
        self.started = False

    def __str__(self):
        return f"The vehicle is a {self.brand}"
    
    def start(self):
        self.started = True
    
    def stop(self):
        self.started = False
```

Essayons d'appeler ces méthodes dans notre fonction *main* :

```python
v.start()
print(v.started)
    
c.start()
print(c.started)
c.stop()
print(c.started)
```

On obtient dans la console :

```
True
True
False
```

On comprend ainsi encore mieux le principe de l'héritage. Comme le terme le laisse suggérer, la classe fille va hériter des membres de la classe mère. Dans notre exemple, la classe `Car` ne possède pas de méthodes `start()` et `stop()`.

De fait, dans le bloc *main*, quand on appelle `c.start()`, c'est la méthode `start()` de la classe `Vehicle` qui est exécutée. Il n'est pas nécessaire de redéfinir toutes les méthodes d'une classe mère dans une de ses classes fille.

Une classe fille peut elle-même être une classe mère. La classe "petite-fille" héritera des membres de sa classe mère et de sa classe "grand-mère" :

```python
class Convertible(Car):

    def __init__(self, brand, wheels_nb):
        super().__init__(brand, wheels_nb)
        self.opened_sunroof = False

    def __str__(self):
        return f"The convertible is a {self.brand}, has {self.wheels_nb} wheels"
    
    def start(self):
        super().start()
        self.open_sunroof()
        
    def stop(self):
        super().start()
        self.close_sunroof()

    def open_sunroof(self):
        self.opened_sunroof = True

    def close_sunroof(self):
        self.opened_sunroof = False
```

En Python, une classe peut avoir plusieurs classes mères :

```python
class Housing:

    def __init__(self, floors_nb, bedrooms_nb):
        self.floors_nb = floors_nb
        self.bedrooms_nb = bedrooms_nb

    def __str__(self):
        return f"The housing has {self.floors_nb} with {self.bedrooms_nb} bedroom(s)"


class CampingCar(Car, Housing):

    def __init__(self, brand, wheels_nb, bedrooms_nb):
        Housing.__init__(self, 1, bedrooms_nb)
        Car.__init__(self, brand, wheels_nb)

    def __str__(self):
        return f"The camping car is made by {self.brand} and has {self.bedrooms_nb} bedrooms"
```

Pour savoir si un objet est d'un certain type, on peut utiliser la méthode `isinstance` :

```python
print(isinstance(camp_car, CampingCar))
```

Pour savoir si une classe est une classe fille d'une autre classe, on utilise la méthode `issubclass`

```python
print(issubclass(Convertible, Vehicle))
```

En résumé, l'héritage permet de designer son projet "en entonnoir". On peut partir d'un objet très général, et en créer de nouveaux, pouvant répondre à des besoins de plus en plus spécifiques.

### Définir ses propres exceptions

L'héritage peut également être utilisé pour définir des exceptions personnalisées. Par exemple :

```python
class SpecificException(Exception):
    pass
```

Il suffit donc de créer une sous classe de la classe de base `Exception`, et il n'est même pas nécessaire de redéfinir le constructeur.

## Surcharge (*overload*) de méthode

Dans d'autres langages de programmation, il est possible d'avoir plusieurs méthodes avec le même nom. En Python, c'est impossible.

Mais il est possible d'obtenir un comportement similaire en utilisant les arguments par défaut pour les paramètres.

[Exemple de la séance 1](../Seance01/README.md#définir-un-argument-par-défaut) :

```python
def power(x, y=2):
    return x**y
```

La fonction va se comporter différemment en fonction du nombre d'arguments reçus en paramètre. Cette technique peut être utile si l'on a l'habitude d'utiliser la surcharge de méthode dans d'autres langages comme le JAVA.

Pour aller plus loin : *packing* et *unpacking* d'arguments avec `*`, `*args` et `**kwargs`

## Exercices

- Circular Buffer
