class Vehicle:

    def __init__(self, brand):
        self.brand = brand
        self.started = False

    def __str__(self):
        return f"The vehicle is a {self.brand}"

    def start(self):
        self.started = True

    def stop(self):
        self.started = False


class Car(Vehicle):

    def __init__(self, brand, wheels_number):
        super().__init__(brand)
        # Vehicle.__init__(self, brand)   # Equivalent to line above
        self.wheels_number = wheels_number

    def __str__(self):
        return f"The car is a {self.brand} and has {self.wheels_number} wheels"


class Convertible(Car):

    def __init__(self, brand, wheels_number):
        super().__init__(brand, wheels_number)
        self.opened_sunroof = False

    def __str__(self):
        return f"The convertible is a {self.brand}, has {self.wheels_number} wheels"

    def open_sunroof(self):
        self.opened_sunroof = True

    def close_sunroof(self):
        self.opened_sunroof = False

    def start(self):
        super().start()
        self.open_sunroof()

    def stop(self):
        super().start()
        self.close_sunroof()


class Housing:

    def __init__(self, floors_nb, bedrooms_nb):
        self.floors_nb = floors_nb
        self.bedrooms_nb = bedrooms_nb

    def __str__(self):
        return f"The housing has {self.floors_nb} with {self.bedrooms_nb} bedroom(s)"


class CampingCar(Housing, Car):

    def __init__(self, brand, wheels_number, bedrooms_number):
        super().__init__(1, bedrooms_number)    # super() references to the first inherited class, here Housing
        # Housing.__init__(self, 1, bedrooms_number)
        Car.__init__(self, brand, wheels_number)

    def __str__(self):
        return f"The camping car is made by {self.brand} and has {self.bedrooms_nb} bedrooms"


def main():
    my_vehicle = Vehicle("Suzuki")
    my_car = Car("Ford", 4)

    print(my_vehicle)
    print(my_car)

    my_vehicle.start()
    print(my_vehicle.started)

    my_car.start()
    # Car.start(my_car)    # do the same thing as line above
    print(my_car.started)
    my_car.stop()
    print(my_car.started)

    my_convertible = Convertible("Renaud", 4)
    my_convertible.start()
    my_convertible.open_sunroof()

    my_camping_car = CampingCar("Rapido", 8, 2)
    print(my_camping_car)

    print(isinstance(my_convertible, Vehicle))  # Will return True
    print(type(my_convertible) is Vehicle)      # Will return False
    print(issubclass(Housing, Vehicle))         # Will return False
    print(issubclass(Convertible, Vehicle))     # Will return True


if __name__ == "__main__":
    main()
