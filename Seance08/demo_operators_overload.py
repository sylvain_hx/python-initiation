UNITS = {"": 0, "kilo": 3, "mega": 6, "giga": 9}


class Storage:
    def __init__(self, value, unit):
        self.value = value
        self.unit = unit
        self.value_without_unit = value * (10**UNITS[unit])

    def __str__(self):
        return f"Storage is {self.value_without_unit} {self.unit}bytes."

    def __add__(self, other):
        if type(other) == str:
            raise TypeError("unsupported operand type(s) for +: 'Storage' and 'str'")
        if type(other) == type(self):
            return Storage(self.value_without_unit + other.value_without_unit, "")
        # if type(other) in (int, float):
        #     return Storage(self.value + other, self.unit)
        if type(other) in (int, float):
            return self + Storage(other, "")

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        if type(other) == type(self):
            return Storage(self.value_without_unit - other.value_without_unit, "")
        if type(other) in (int, float):
            return self - Storage(other, "")

    def __rsub__(self, other):
        if type(other) == type(self):
            return Storage(other.value_without_unit - self.value_without_unit, "")
        if type(other) in (int, float):
            return Storage(other, "") - self

    def __eq__(self, other):
        if type(other) != type(self):
            return False
        if type(other) == type(self):
            return self.value_without_unit == other.value_without_unit

    def __gt__(self, other):
        if type(other) != type(self):
            raise TypeError("'>' not supported between instances of 'Storage' and 'int'")
        return self.value_without_unit > other.value_without_unit


def main():
    ssd_1kilo = Storage(1, "kilo")
    ssd_2mega = Storage(2, "mega")
    merged_ssd = ssd_1kilo + ssd_2mega
    print(merged_ssd)
    print(ssd_1kilo + 10)
    # print(ssd1 + "hello")     # Will raise an error
    print(4 + ssd_1kilo)
    print(ssd_2mega + ssd_1kilo)
    print(ssd_1kilo - 10)
    print(1000000 - ssd_1kilo)
    print(1000 == ssd_1kilo)
    print(ssd_1kilo == Storage(1, "kilo"))
    print(ssd_1kilo == ssd_2mega)
    print(ssd_1kilo > ssd_2mega)
    # print(ssd_2mega > 20)   # Will raise an error


if __name__ == '__main__':
    main()
