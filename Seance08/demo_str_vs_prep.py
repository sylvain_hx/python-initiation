class Student:
    def __init__(self, first_name, last_name, group):
        self.first_name = first_name
        self.last_name = last_name
        self.group = group

    def __str__(self):
        return (f"Student is called {self.first_name} {self.last_name} "
                f"and is in group {self.group}")

    def __repr__(self):
        return f"Student(\"{self.first_name}\", \"{self.last_name}\", \"{self.group}\")"


def main():
    sylvain = Student("Sylvain", "Huraux", "EN5")
    print(sylvain)
    print(repr(sylvain))


if __name__ == '__main__':
    main()
