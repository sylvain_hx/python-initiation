for n in range(10):
    print(f"n = {n}")
print("Program exited the for loop\n")

languages = ["C", "C++", "Java", "Python", "HTML", "CSS", "PHP", "Go", "Rust"]
for language in languages:
    print(language)
print()

for i, language in enumerate(languages):
    print(f"{i} : {language}")
print()

for i in range(5, 500, 5):
    if i == 300:
        break
    if i % 10 == 0:
        continue
    print(i)
print("end of program")
