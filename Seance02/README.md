# Séance 2

## Installer un environnement de développement : PyCharm Community Edition

Généralement, lorsque l'on souhaite développer (c'est-à-dire écrire du code, programmer), on utilise un environnement de développement (IDE en anglais). Il s'agit d'un logiciel dont le cœur sera bien sûr un éditeur de texte, mais qui possédera beaucoup d'autres fonctionnalités bien utiles pour être plus efficace et travailler plus confortablement.

Il existe plusieurs IDE, pour différents langages et ayant chacun des spécificités différentes. Dans le cadre de ce séminaire, nous utiliserons PyCharm. La version *Community* est gratuite (et *open-source*, comme Python). Lien pour télécharger : https://www.jetbrains.com/pycharm/download/

*NB* : vous pouvez obtenir gratuitement une license pour utiliser la version payante de *PyCharm* (et d'autres logiciels de *JetBrains*) en créant un compte sur le site avec votre email HE2B, puis en demandant une clé de license éducation.

Au premier lancement de PyCharm, vous pourrez créer un nouveau projet. Un projet *PyCharm* n'est qu'un simple dossier. Le logiciel vous demandera où vous souhaitez créer ce projet (par défaut, *PyCharm* créera un dossier "PycharmProjects" dans votre dossier utilisateur, et ajoutera le nouveau projet dedans). Libre à vous de choisir un autre emplacement). Cliquez sur "créer" pour terminer, en ne modifiant pas les autres paramètres.

À ce moment, l'interface de *PyCharm* change. Le projet nouvellement créé est ouvert.

Dans la colonne de gauche se trouve l'explorateur de projet. Pour l'instant, vous pouvez voir qu'il n'existe qu'un seul fichier Python dans votre projet/dossier. Il a été créé automatiquement par PyCharm et s'appelle *main.py* (*.py* étant l'extension des fichiers Python).

Au centre se trouve l'éditeur de code.

En haut à droite, se trouve un bouton *play*. Cliquez dessus. Le script *main.py* vient d'être exécutée. Un nouvel espace est normalement apparu en bas de la fenêtre. Il s'agit de la sortie console du script. C'est dans cet espace que sera affiché ce qui est envoyé en entrée des fonctions `print()`, et ce qui permettra à l'utilisateur·ice d'envoyer des caractères au programme via la fonction `input()`. 

### Créer un premier projet PyCharm

Le script *main.py* est un "*Hello World*", c'est-à dire un programme de base dont le seul but est d'afficher le message "Hello world". Ajoutons-maintenant un autre script à notre projet. Pour cela, faites clic droit sur le dossier de projet en haut à gauche (appelé *pythonProject* si vous avez laissé le nom de projet par défaut) et faites nouveau fichier → fichier Python. Nommez-le par exemple *demo*.

Ce fichier est automatiquement ouvert dans l'éditeur. Il est bien évidemment vide. Vous pouvez aussi constater qu'il y a maintenant deux onglets dans l'éditeur de code. Vous pouvez donc facilement passer d'un fichier à un autre. Il faut bien comprendre que lorsque vous écrivez dans l'éditeur, le fichier *.py* est automatiquement édité. Il n'y a pas besoin de sauvegarder ce qu'on écrit

Faites clic droit sur l'onglet de *demo1.py* puis *lancer 'demo1'*

La console en bas de PyCharm affiche maintenant une première ligne (vous indiquant quel fichier/script vient d'être interprété) puis le message `hello world`. Enfin, en dessous se trouve une ligne vous indiquant que l'exécution du script s'est terminée.

### Le *REPL* de Pycharm

Vous avez accès à un *REPL* dans *PyCharm*. Il est accessible via un onglet de l'espace en bas de l'IDE (le même que celui affichant la sortie). S'il n'y a pas d'onglet "Console Python", alors ouvrez le menu de *PyCharm* et allez dans *View* → *Tool Windows*.

Ce *REPL* présente l'avantage d'afficher quelles sont les variables déclarées dans la session en cours, et vers quel objet celles-ci référencent.

### L'aide à la rédaction de PyCharm

Lorsque vous allez commencer à utiliser PyCharm, vous verrez que votre code sera parfois souligné avec différentes couleurs. Ces ajouts sont là pour vous indiquer qu'il y a des problèmes plus ou moins critiques dans votre code

- Souligné en rouge : erreurs. Marque du code qui engendrera une erreur à l'exécution.
- Souligné en jaune : avertissements. Marque du code qui semble "peu pertinent". Par exemple, si vous réassignez une valeur à une variable `x` sans avoir utilisé la valeur précédemment assignée.
- Souligné en vert : fautes typographiques. Indique qu'il y a une "faute d'orthographe" dans le nom d'une variable, fonction.
- Souligné en gris : non-respect des conventions PEP (*Python Enhancement Proposals*)
- Surligné en gris : Marque du code qui n'engendrera pas systématiquement une erreur, mais qui peut quand même en engendrer une. Par exemple si le code fait référence à une variable qui peut ne pas exister dans certains cas.

### Le débugueur

Un débugueur est un outil permettant de déboguer son programme. Déboguer signifie chercher et corriger les erreurs de son programme.

En Python, un débogueur permet de tester son programme étape par étape, afin de voir le "chemin" qui suit l'interpréteur et l'évolution de ses variables. Pour déboguer, il faut cliquer sur le petit insecte vert à côté du bouton "run" en haut à droite.

Testez avec n'importe quel fichier. Vous ne devriez constater aucune différence par rapport à un simple *run*. En effet, il faut ajouter au moins un *breakpoint* sur son code. Pour ajouter un *breakpoint* dans un fichier, cliquez à gauche d'une ligne de code. Insérez-en un à la première ligne de votre fichier. Un cercle rouge apparait.

Relancez le *debug*. Cette fois l'onglet "*Debug*" s'ouvrira en bas à la place de l'onglet "*run*". En effet l'interpréteur s'est arrêté au premier *breakpoint* du fichier, donc à la première instruction.

En haut de la fenêtre de *debug* se trouvent plusieurs icônes bleues avec des flèches. Le bouton *Step Into* permet d'exécuter les instructions une par une, et ainsi de voir le "chemin" suivi par le programme.

Vous pourrez voir à droite de la fenêtre l'évolution des variables et des objets.

Si vous appuyez sur le bouton "*Resume*" à gauche de la fenêtre, alors le débugueur ira jusqu'au prochain *breakpoint*. Ici, comme vous n'en avez pas placé d'autres, le programme sera exécuté jusqu'à la fin.

Utiliser un débugueur permet d'analyser en détail le fonctionnement de son programme, pas à pas, et donc de mieux détecter les problèmes.

## Les structures conditionnelles : `if`, `elif`, `else`

La plupart du temps, lorsque l'on écrit un programme, celui-ci doit se comporter différemment en fonction de différentes conditions. C'est là qu'interviennent les structures conditionnelles.

Créez un nouveau fichier Python, appelé "demo_if.py". Ajoutez les lignes suivantes :

```python
a = 5
if a == 5:
    print("a is equal to 5")
```

Lancez ce script. La console affiche : `a is equal to 5`

La première ligne affecte à la variable `a` la valeur 5
En dessous se trouve la structure conditionnelle. Le mot clef `if` ("si" en français) marque le début du bloc d'instruction. On retrouve donc le symbole `:` en fin de ligne, comme pour la définition de fonctions.

Dans notre exemple, a est bien égal à 5 L'opération de comparaison `, alors l'opération renvoie `True`, et le code placé dans ce bloc sera exécuté. Soit ici, la ligne avec la fonction `print()`.

En python, pour placer des instructions dans un bloc, il faut indenter la ligne (avec 4 espaces ou une tabulation). Vous voyez qu'ici, le `print()` n'est pas au même niveau que les lignes du dessus, mais décalé "une fois" vers la droite.

Changez la valeur de `a` dans votre code et ajoutez la ligne suivante en dessous, sans l'indenter :

```python
print("print outside the if bloc")
```

La console affichera uniquement le message `print outside the if bloc`. En effet, cette fois, la condition `a == 5` n'est pas vérifiée. L'opération renvoie donc `False`, et l'instruction `print("a is equal to 5")` n'est pas exécutée.

Par contre, la ligne suivante, qui ne fait pas partie de la structure conditionnelle, est bien exécutée.

Pour réaliser des structures conditionnelles avec plusieurs "chemins" possibles, on utilise les mots clefs `elif` (pour *else if*, "sinon si" en français) et `else` ("sinon" en français).

Ajoutez ce code à la suite de votre script : 

```python
b = 10
if b == 10:
    print("b is 10")
elif b == 15:
    print("b is 15")
else:
    print("b is neither 10 nor 15")
```

La console affiche `b is 10`. Testez le programme en changeant la valeur de `b`.

En mettant `b = 15`, alors c'est la deuxième condition (celle du `elif`) qui est vérifiée. Le message affiché sera donc `b is 15`. Si vous fixez `b` à n'importe quelle autre valeur, ainsi c'est l'instruction du bloc `else` qui est exécutée.

Maintenant, ajoutez le code suivant : 

```python
c = 20
if c > 10:
    print("c is strictly greater than 10")
elif c > 5:
    print("c is strictly greater than 5")
```

Ce bout de code va faire afficher `c is strictly greater than 10`, mais pas `c is strictly greater than 5`. Car dans une structure conditionnelle `if..elif..else`, l'ordre a son importance. Si la première condition est vérifiée, alors toutes celles qui viennent ensuite ne sont pas analysées et les instructions des blocs suivants ne seront pas exécutées.

Une structure conditionnelle commence donc toujours par un bloc `if`, suivi de zéro, un ou plusieurs bloc(s) `elif` pour se terminer (mais pas obligatoirement) par un bloc `else`.

Si on remplace le `elif` par un `if` dans le bout de code ci-dessus, alors on obtient deux structures conditionnelles indépendantes. La console affichera cette fois : 

```
c is strictly greater than 10
c is strictly greater than 5
```

Notez enfin, qu'il est tout à fait possible d'imbriquer un bloc de code dans un autre. Il est donc possible d'avoir une structure conditionnelle dans un bloc d'une structure. Exemple :

```python
d = 12
if d > 5:
    print("d > 5")
    if d > 10:
        print("d > 10")
```

On repère facilement les différents "niveaux" dans le code grâce à l'indentation. Ici, si `a` est supérieur à 10, les deux `print()` seront affichés dans la console. Par contre, si `a` est compris entre 5 (exclus) et 10 (exclus), la console n'affichera que `a > 5`.

## Les séquences

Le terme séquence désigne les types composites (ou collections) étant des "regroupements" d'objets ordonnés, (comme une série en maths). Chaque élément d'une séquence possède un indice, qui correspond à sa position dans la séquence.

Python propose 3 types *built-in* de séquences : `str`, `list`, `tuple`.

`str` est bien une séquence. Nous avions vu dans la séance 1 qu'il était possible de récupérer seulement un caractère d'une chaîne. Le principe sera le même pour les autres types de séquence.

### Le type `list`

Une liste est une séquence modifiable (mutable) d'objets. On utilise les crochets `[]` pour créer une liste :

```python
l1 = [1, 2, 3.5, "hello", True, "red", 3.5, 5.5445, "b"]
```

Une liste peut contenir différents types d'objets. Ici, `l1` contient aussi bien un `int`, un `float`, un `str` ou un `bool`.

Pour accéder aux éléments d'une liste, il faut écrire la variable référençant vers cette liste, suivi de l'**indice** (la position) entre crochets. 

**RAPPEL** : on commence à "compter" les éléments à partir de 0 !

Exemples :

```python
a = l1[0]       # Get the first item of the list
b = l1[2:7]     # Get items from 3rd to 7th (item of index 7 is excluded)
c = l1[4:]      # Get items for 5th to last
d = l1[:5]      # Get items from 1st to 4th
e = l1[-2]      # Get the penultimate item
f = l1[1:7:2]    # Get the 2nd 4th and 6th of the list (2 is the step)
```

Une liste est modifiable. On peut donc en modifier ses éléments :

```python
l1[0] = 100     # Modify the 1st item
l1[2:5] = [50, 60, 70]  # Modify the 3rd, 4th and 5th items
```

Il est aussi possible d'ajouter de nouveaux éléments dans une liste existante.

```python
l1.append("important message")      # Add the item at the end
l1.insert(2, "220")     # Add the item between the 2nd and the 3rd
l2 = [6, 7, 8.6, "xyz"]
l1.extend(l2)           # Add the items of l2 list at the end of l1 list
```

Que signifie cette nouvelle syntaxe ; `variable_name.function()` ? Il s'agit en fait de la syntaxe pour appeler une *méthode* (une fonction d'un objet) d'instance.

Les méthodes `append()`, `insert()` et `extend()` vont "travailler" sur la liste référencée par `l1.

Les méthodes sont des fonctions, mais qui opèrent donc sur un objet, là où les fonctions comme `print()` ou `input()` s'utilisent "toutes seules" (et ne sont donc pas appelées méthodes). Nous expliquerons ce concept plus en détail dans la partie sur la POO.

Voici comment supprimer des éléments d'une liste :

```python
l1.remove("important message")  # Remove the item : "important message"
l1.pop(4)   # Remove the 5th item
l1.clear()  # Empty the list
```

Si vous voulez avoir plus d'information sur un type et ses méthodes, n'hésitez pas à aller voir la documentation en exécutant `help(list)` dans une console interactive.

Pour connaître le nombre d'éléments d'une liste, on utilise la fonction `len()` :

```python
l1_length = len(l1)
```

Pour savoir si un élément est présent dans une liste, on utilise le mot-clef `in` :

```python
is_present = 6 in l1
```

Dans cet exemple, la variable `is_present` reverra vers `False`, car l'entier `6` n'est pas présent dans la liste `l1`.

Voici comment copier une liste :

```python
l3 = l1.copy()
```

**NB** : la méthode `copy()` créer une nouvelle liste en mémoire, qui sera une copie de celle référencée `l1`. En revanche, si vous faites :

```python
l4 = l1
```

Vous ne créez pas une nouvelle liste, mais vous faites simplement référencer `l3` vers la même liste que `l1`. Les deux variables référenceront donc vers la même liste.

Vous pouvez vérifier vers quel objet référence une variable avec la fonction `id()` : exemple :

```python
print(id(l1))
print(id(l3))
print(id(l4))
```

Les variables `l1` et `l4` référencent bien vers le même objet, l'id renvoyé étant identique. En revanche `id(l3)` renvoie bien comme prévu un id différent.

### Les listes multidimensionnelles

Une liste est une séquence d'objets. Il est donc possible qu'une liste contienne des listes, et donc de créer une liste à plusieurs dimensions :

```python
matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
coefficient = matrix[0][2]
```

la variable `coefficient` vaut `3` (première ligne : indice 0, 3e colonne : indice 2).

### Le type `tuple`

Un tuple est une séquence non modifiable (immuable ou *immutable* en anglais). C'est donc comme une liste, mais qui ne pourra plus être modifié après son instanciation.

Pour créer une liste, on utilise les parenthèses `()` :

```python
t = (1, 2, 3, 5, 6.5, "ok", False, "some text")
```

Pour récupérer un (ou des) élément(s) du tuple, on fait comme avec une liste :

```python
f = t[3]    # Get the 4th item of the tuple
```

Comme un tuple est immuable, on ne peut ni modifier des éléments du tuple, ni en ajouter ou en retirer.

Quel est l'intérêt d'utiliser un tuple, si l'on peut faire la même chose avec une liste ?

D'une part, créer une séquence "protégée". Dans certains cas, on peut souhaiter qu'une séquence reste constante. La stocker dans un tuple permet d'être sûr que celle-ci ne pourra pas être modifiée.

Le fait qu'un tuple soit immuable offre aussi un gain de performance. Si vous souhaitez effectuer beaucoup d'opérations à partir d'une séquence, utiliser un tuple permet d'avoir un programme plus rapide.

Néanmoins, pour des opérations mathématiques plus complexes, il vaudra mieux utiliser des librairies dédiées, comme `numpy` et `pandas`.

### Le type `range`

Le type `range` sert à créer une séquence de nombres entiers. Voici comment créer un `range` :

```python
r1 = range(10)      # Create a range from 0 to 10 excluded, with a step of 1
r2 = range(2, 10)   # Create a range from 2 to 10 excluded, with a step of 1
r3 = range(5, 50, 5)   # Create a range from 5 to 50 excluded, with a step of 5
```

La syntaxe `range()` appelle le **constructeur** de la classe `range`.

On peut en effet créer tout type d'objet de cette manière, par exemple :

```python
li = list((1, 2, 5, 10.5, "green"))     # Create a list from a tuple
t = tuple([1, 4, 4.5, "ok", "ok"])      # Create a tuple from a list
x = int(4.0)                              # Create an int from a float
```

## Les boucles itératives

### La boucle `while`

Dans la plupart des programmes informatiques, il est nécessaire d'effectuer plusieurs fois la même action, donc effectuer quelque chose "en boucle". On parle également d'itérations, et ainsi de boucle itérative.

Une des deux manières de faire en Python est la boucle `while` ("tant que" en français). Comme son nom le laisse supposer, elle sert à effectuer une action tant que sa condition d'entrée est `True`.

Une boucle `while`, comme pour les `if`, `elif` et `else`, s'écrit en tant que bloc d'instructions :

```python
n = 0
while n < 10:
    print(f"n = {n}")
    n += 1  # Assignment and modification in 1 instruction : equivalent to n = n + 1
print("Program exited the while loop")
```

Créez un nouveau script Python appelé "demo_while.py". Écrivez-y le code ci-dessus et exécutez-le. Vous devriez obtenir la chose suivante dans la console :

```
n = 0
n = 1
n = 2
n = 3
n = 4
n = 5
n = 6
n = 7
n = 8
n = 9
Programme sorti de la boucle while
```

Les instructions à l'intérieur de la boucle ont été exécutées 10 fois. En effet, lorsque l'interpréteur arrive pour la première fois à la ligne 2 du code, la condition `n < 10` est `True` (car `n` est égal à 0). Le programme "rentre" dans la boucle et exécute les deux lignes.

NB : la deuxième ligne de la boucle contient un opérateur d'affectation particulier : il sert à "mettre à jour" la valeur référencée par la variable `n`. À la fin de cette première itération, `n` vaut 1.

Là, le programme retourne au début de la boucle et vérifie à nouveau si la condition est `True`. C'est toujours le cas, donc les instructions de la boucle sont à nouveau exécutées. Et ainsi de suite, 10 fois.

À la fin de la dixième itération, `n` vaut 10. Par conséquent, quand le programme retourne vérifier la condition d'entrée de la boucle, celle-ci renvoie `False`, car `n` n'est pas strictement inférieur à 10. Le programme ne rentre donc pas dans la boucle et va continuer en exécutant l'instruction suivante, en l'occurrence `print("Programme sorti de la boucle while)`.

### La boucle `for`

Là où dans d'autres langages, comme en C, la boucle `for` est une sorte de boucle `while` "améliorée", en Python, la boucle `for` est ce qu'on appelle une boucle *for each*. Elle sert à parcourir des types composites, comme des listes, des tuples, des sets...

Écrivons par une boucle `for` qui ferait la même chose que la boucle `while` vu séance 2 :

```python
for n in range(10):
    print(f"n = {n}")
print("Program exited the for loop")
```

`n` va successivement prendre les valeurs allant de 0 à 9, c'est-à-dire les valeurs de l'objet de type `range`.

Autre exemple :

```python
languages = ["C", "C++", "Java", "Python", "HTML", "CSS", "PHP", "Go", "Rust"]
for language in languages:
    print(language)
```

Ici, la variable `langage` va prendre toutes les valeurs successives de la liste `langages`, soit `"C"`, puis `"C++"`... jusqu'à `"Rust"`.

Il est aussi possible de récupérer en même temps l'élément et l'indice correspondant, grâce à la fonction `enumerate()` :

```python
for i, language in enumerate(languages):
    print(f"{i} : {language}")
```

Il est possible "d’altérer" une boucle avec deux mots clefs du langage : `break` et `continue`.

- `break` permet de sortir d'une boucle, sans terminer l'itération en cours.
- `continue` permet de terminer une itération en cours, sans exécuter les instructions qui suivent.

Exemple :

```python
for i in range(5, 500, 5):
    if i == 300:
        break
    if i % 10 == 0:
        continue
    print(i)
```

Si vous exécutez cette boucle, vous verrez que les multiples de 10 ne seront pas affichés dans la console. Car lorsque `i % 10 == 0`, l'instruction `continue` est exécutée, et donc l'instruction `print(i)` n'est pas exécuté.

De plus, le dernier nombre affiché sera `295`. Lorsque la condition `i == 300` est vraie, alors l'instruction `break` est exécutée, ce qui fait directement sortir le programme de la boucle `for`, sans passer par le `print(i)`

## Les exceptions

Lorsqu'on écrit un programme, on passe son temps à la tester. Ceux-ci seront souvent générateurs d'erreurs : mauvaise syntaxe, donnée mauvaise ou manquante... Dans tous les cas, une erreur interrompt le programme, car celui-ci ne peut pas ou plus obtenir les informations nécessaires pour continuer son exécution.

En Python, on fait la distinction entre erreur de syntaxe (que vous avez vraisemblablement déjà croisé en faisant des exercices) et exception.

Une erreur de syntaxe est une erreur d'écriture de votre code Python : oubli de parenthèses, mauvaise indentation du code... Cela est lié à l'écriture du code en lui-même, mais pas à un "mauvais" fonctionnement.

Le reste des erreurs sont des exceptions. Par exemple :

```python
a = 50
result = a / 0
```

Ce code lancera une exception, car l'interpréteur ne peut pas diviser un nombre par zéro.

```python
s = "hello"
x = s + 40
```

Ce code lancera un autre type d'exception, car on ne peut pas "additionner" un type `string` avec un type `int`.

Pour l'instant, nous n'avons fait que recevoir des erreurs, nous indiquant que quelque chose n'est pas correct dans la conception de notre code. Mais les exceptions peuvent aussi être interceptées, et que le programme agisse en conséquence et puisse continuer à fonctionner.

### `try except` : Interception (*catching*) des exceptions

Le mot clef `try` permet de délimiter un bloc de code dans lequel, si une exception est lancée, celle-ci sera détectée et interceptée. Le mot clef `except`, qui l'accompagne forcément, correspond au bloc de code qui sera exécuté si une exception a été lancée par du code contenu dans le bloc `try`

```python
try:
    s = "hello"
    x = s + 40
except:     # BAD PRACTICE : too broad exception clause
    print("Exception detected !")

print("Exception has been handled, program can continue")
```

En exécutant ce script, aucun message d'erreur en rouge n'apparait dans la console. À la place, c'est le message du `print` contenu dans le `except` qui apparait. Le programme continue ainsi son exécution.

Évidemment, il n'est pas du tout recommandé d'intercepter toutes les exceptions comme ci-dessus. Cela revient à négliger toutes les erreurs. L'intérêt est de pouvoir détecter des exceptions spécifiques :

```python
try:
    n = 50
    d = int(input("Enter the denominator value"))
    result = n / d
except ValueError as e:
    print(f"{e} : You haven't entered a number for the denominator")
except ZeroDivisionError as e:
    print(f"{e} : Division can't be done. Denominator has been set to 0")
```

Ce code peut agir différemment en fonction des deux exceptions possibles : soit les caractères rentrés ne peuvent pas être convertis en un nombre, soit ce nombre vaut 0.

Notez que l'ordre a une importance : certaines exceptions plus spécifiques sont dérivées d'autres exceptions. Si une exception est interceptée par un premier `except` , elle ne pourra pas être re-détectée par un `except` en dessous. Il faut donc classer ses `except` du plus au moins spécifique.

Ce fonctionnement est dû au principe d'héritage de la programmation orientée objet, qui sera abordé plus tard.

### `else` et `finally`

Le mot clef `else` permet de définir du code qui sera exécuté si aucune erreur n'est levée. Le mot clef `finally` permet de définir du code qui sera exécuté dans tous les cas.

```python
try:
    n = 50
    d = int(input("Enter the denominator value"))
    result = n / d
except ValueError as e:
    print(f"{e} : You haven't entered a number for the denominator")
except ZeroDivisionError as e:
    print(f"{e} : Division can't be done. Denominator has been set to 0")
else:
    print(f"result of the division is {result}")
finally:
    print("finally, it is the end of this course")
```

### Lever ses propres exceptions

Il est possible de lever ses propres exceptions directement dans son code source, permettant de détecter par la suite telle ou telle erreur.

Essayons de lever une exception à l'intérieur d'une fonction :

```python
def divide_x_by_y(x, y):
    if y == 0:
        raise ZeroDivisionError("Custom message : you can't divide a number by zero")
    return x/y
```

Dans cette fonction, la ligne `raise ZeroDivisionError("Custom message : you can't divide a number by zero")` lève (*raise*) en anglais, l'exception de type `ZeroDivisionError`

Ajoutons maintenant le code suivant dans le bloc principal :

```python
try:
    x, y = 10, 2
    division_result = divide_x_by_y(x, y)
    print(f"Result of {x} divided by {y} is {division_result}")
    y = 0
    division_result = divide_x_by_y(x, y)
except ZeroDivisionError as e:
    print(e)
```

Lorsqu'une exception est levée, le programme sort instantanément du contexte courant (dans cet exemple la fonction `divide_x_by_y()`) pour remonter au contexte supérieur, et ainsi de suite jusqu'à "tomber" sur un bloc `try except` pouvant potentiellement gérer cette exception. Si aucun bloc `except` ne peut l'intercepter, alors le programme s'arrête et la console affiche qu'une erreur s'est produite.

Il existe beaucoup d'exceptions *built-in* en Python. Consultez la documentation Python pour plus d'information à ce sujet.

## Exercices :

- Meltdown Mitigation
- Little Sister's Vocabulary
- Black Jack
- Card Games
- Triangle
- Grains
- Armstrong Numbers
- Collatz Conjecture
- Raindrops
- Darts
- Perfect Numbers
- Nth Prime
