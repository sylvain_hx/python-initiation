def divide_x_by_y(x, y):
    if y == 0:
        raise ZeroDivisionError("Custom message : you can't divide a number by zero")
    return x/y


if __name__ == "__main__":
    try:
        s = "hello"
        x = s + 40
        print("Should not be printed anyway")
    except:
        print("Exception detected !")

    print("Exception has been handled, program can continue")

    try:
        n = 50
        d = int(input("Enter the denominator value : "))
        result = n / d
    except ValueError as e:
        print(f"{e} : You haven't entered a number for the denominator")
    except ZeroDivisionError as e:
        print(f"{e} : Division can't be done. Denominator has been set to 0")
    else:
        print(f"Result of the division is {result}")
    finally:
        print("Finally, it is the end of this try-except scope")

    try:
        x, y = 10, 2
        division_result = divide_x_by_y(x, y)
        print(f"Result of {x} divided by {y} is {division_result}")
        y = 0
        division_result = divide_x_by_y(x, y)
    except ZeroDivisionError as e:
        print(e)
