# Initiation à la programmation : langage Python

Un langage de programmation est, comme son nom l'indique, un langage permettant de "parler" avec une machine. Le terme machine peut désigner toutes sortes d'appareils "informatiques" : un ordinateur, un smartphone, un objet connecté...

Pour être exact, écrire un programme "donne des ordres" à une machine, que celle-ci devra ensuite exécuter.

Un ordinateur n'est pas "intelligent". Il ne fait qu'effectuer ce qu'un programme lui demande de faire.

Il existe de nombreux langages de programmation, ayant chacun leur domaine de prédilection et leurs particularités.

Parmi les plus utilisés, on peut citer le C/C++, le C#, le Java, le JavaScript, et donc, le Python.

Le contenu de ce cours est découpé en séances. Celles-ci sont à prendre dans l'ordre chronologique :

## Labo 1

[Séance 1: introduction, éditeur de code, invite de commande, *REPL*, variables, types de base, opérateurs, fonctions](Seance01/README.md)

[Séance 2: PyCharm, structures if elif else, listes, tuples, range, boucle while, boucle for, exceptions](Seance02/README.md)

[Séance 3: fonctions *built-in*, méthodes de str et de list, opérateur ternaire, *list comprehensions*](Seance03/README.md)

[Séance 4: sets (et ses méthodes), dictionnaires (et ses méthodes)](Seance04/README.md)

[Séance 5: modules *built-in*, écriture de modules, fonction main() et condition if \_\_name\_\_ == "\_\_main\_\_"](Seance05/README.md)

[Séance 6: lecture et écriture de fichiers: texte, *JSON*, *CSV*](Seance06/README.md)

## Labo 2

[Séance 7: Programmation orientée objet, classes, constructeur](Seance07/README.md)

[Séance 8: méthodes spéciales (*dunder/magic methods*) (TODO)](Seance08/README.md)

[Séance 9: héritage](Seance09/README.md)

[Séance 10: décorateurs, membres de classes, méthodes statiques](Seance10/README.md)

[Séance 11: membres protégés, @property](Seance11/README.md)

[Séance 12: Révisions](Seance12/README.md)

## Labo 3

[Séance 13: *Data Science* : numpy, pandas](Seance13/README.md)

[Séance 14: exercice pandas supplémentaire](Seance14/README.md)

[Séance 15: matplotlib](Seance15/README.md)

[Séance 16: Base de données relationnelles, serveur MySQL/MariaDB, langage SQL (TODO)](Seance16/README.md)

[Séance 17: SQL en Python: SQLite, MySQL/MariaDB (TODO)](Seance17/README.md)

[Séance 18: ? (TODO)](Seance18/README.md)

## Exercices à faire

Les exercices à faire (pour les séances 1 à 10) sont ceux du site [Exercism.org, "*track*" Python](https://www.exercism.org/tracks/python). Il faut simplement se créer un compte (gratuit) avec une adresse email.

## Documentation, tutoriels, vidéos et playlists

### Documentation officielle

La [documentation officielle Python 3](https://docs.python.org/3) est consultable en ligne. Elle fournit une vue exhaustive des fonctionnalités de tel ou tel élément.

**À noter** : vous pouvez consulter la documentation Python officielle de n'importe quel élément `element` (qui peut être une fonction, un module, une classe...) même hors-ligne, en appelant la fonction `help(element)` dans une console interactive Python (*REPL*).

### Real Python

Le site [RealPython.com](https://realpython.com) reste la référence incontournable en matière d'articles/tutoriels. Une grande partie du contenu est gratuit, mais certains tutoriels plus avancés sont payants (le site fonctionne sur un système d'abonnement mensuel/annuel).

### Autres sites avec des articles/tutoriels clairs :

- www.w3schools.com/python
- www.geeksforgeeks.org/python-programming-language
- www.tutorialstonight.com/python/python-tutorial
- www.tutorialsteacher.com/python
- www.programiz.com/python-programming
- www.docstring.fr (en français)

### YouTube :

- [Indently](https://www.youtube.com/@Indently)
- [NeuralNine](https://www.youtube.com/@NeuralNine)
- [TechWithTim](https://www.youtube.com/@TechWithTim)
- [CoreyMS](https://www.youtube.com/@coreyms)
- [Playlist de Socratica](https://youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-&si=q76145NzEB29amUF)
- [Playlist de FreeCodeCamp.org](https://youtube.com/playlist?list=PLWKjhJtqVAbkmRvnFmOd4KhDdlK1oIq23&si=2h9QaBvgJmII1_S6)
- [Docstring](https://www.youtube.com/@Docstring) (en français)

### Autres sites avec des exercices :

- www.w3resource.com/python-exercises
- www.practicepython.org/exercises
- www.github.com/geekcomputers/Python
- www.docstring.fr/formations/exercices (en français)
