# Séance 7

## La Programmation Orientée Objet : introduction

POO : Programmation Orientée Objet

La POO est un paradigme : c'est-à dire une "manière" d'appréhender le fonctionnement de son programme. Jusqu'ici, vos programmes fonctionnaient de façon structurée : fonctionnement séquentiel, conditionnel, répétitif, avec des fonctions.

La POO, comme son nom l'indique, consiste à considérer son programme non pas comme un ensemble de fonctions, mais comme un ensemble d'objets. Un objet possède des caractéristiques, des états et des comportements.

Nous avons déjà dit qu'en Python, **tout est objet**. Un `int`, un `float`, une `list`... sont des objets. L'intérêt de la POO, c'est de pouvoir définir ses propres objets, grâce aux classes.

### Les classes

Une classe, c'est le "plan" (*blueprint* en anglais) d'un objet. Cela énumère les caractéristiques et fonctionnalités qu'un objet issu de cette classe va posséder.

On peut faire l'analogie avec un patron en couture. Un patron vous donne les dimensions des différents morceaux de tissus, où coudre, plier... pour créer votre vêtement. Mais ce n'est pas le vêtement en lui-même. Et à partir d'un même patron, vous pouvez créer plusieurs fois le même vêtement, tout en y ajoutant à chaque fois des variations : il pourra être bleu et en coton, rouge et en acrylique, vert et en lin...

La classe, c'est le patron du pantalon et l'objet, c'est le pantalon noir en coton.

Formulé autrement, on dira que le pantalon noir en coton est une instance de la classe pantalon.

Et on peut aussi dire que l'objet pantalon noir en coton est du type pantalon.

Les termes classe et type désignent en fait la même chose, idem pour les termes objets et instance. En pratique, on utilisera les uns ou les autres en fonctions du contexte.

En POO, les caractéristiques sont appelées **attributs**. Toujours pour notre classe pantalon, on aura l'attribut couleur et l'attribut matière.

Les comportements d'un objet sont ses fonctionnalités, donc des fonctions. Les fonctions d'une classe sont appelées **méthodes**. Pour un pantalon, on peut imaginer par exemple les méthodes ouvrir la braguette, fermer la braguette, ranger dans la poche droite...

Voici comment écrire cette classe en Python :

```python
class Trouser:

    def __init__(self, color, material):
        self.color = color
        self.material = material
        self.zip_opened = False

    def __str__(self):
        return f"The trouser is {self.color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    def open_zip(self):
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False

    def dye(self, color):
        self.close_zip()
        self.color = color
```

Le mot clef `class` permet de définir le nom de la classe. NB : la convention de nommage des classes n'est pas la même que pour les variables ou les fonctions. Tous les mots doivent commencer par une majuscule, sans êtres séparés par un `_`.

Cette classe contient quatre méthodes :

- `__init__(self, color, material)` : **Constructeur**. C'est cette méthode qui est exécutée lorsque l'on souhaite créer un nouvel objet de type `Trouser`.
- `__str__(self)` : méthode pour le `print`. Renvoie une chaine de caractères, qui sera affiché lorsque l'on fait `print(trouser_name)`.
- `open_zip(self)` : modifie l'attribut de l'objet, pour "ouvrir la braguette"
- `close_zip(self)` : pour fermer la braguette.
- `dye(self, color)` : pour changer la couleur du pantalon

### Instancier une classe : créer un objet

Ajoutons un `main` à notre script pour créer des instances de cette classe :

```python
if __name__ == "__main__":
    blue_denim = Trouser("blue", "denim")
    brown_chino = Trouser("brown", "chino")
    print(brown_chino.color)  # Access value of attribute material of the object referenced by brown_chino variable
    print(brown_chino)
    blue_denim.open_zip()   # Executes open_zip() method of object referenced by blue_denim variable
    print(blue_denim)
    blue_denim.dye("black")
    print(blue_denim)
```

Les variables `blue_denim` et `brown_chino` sont deux variables référençant vers deux objets différents, ayant des caractéristiques différentes, mais qui sont tous deux de type `Trouser`.

Comme expliqué plus haut, ces deux commandes vont donc appeler la méthode `__init__()` de la classe. Cette méthode sert à créer l'objet, et à initialiser les valeurs des attributs, qui sont dans cet exemple :

- `color`, pour représenter la couleur du pantalon
- `material`, pour représenter la matière
- `opened_zip`, pour représenter l'état de la fermeture Éclair (fermé ou ouvert)

La troisième instruction affichera `brown`, soit la valeur de l'attribut `color`.

La quatrième ligne exécute la méthode `__str__()`.

La cinquième ligne appelle la méthode `open_zip()` sur l'objet `blue_denim`. On voit bien grâce au `print` de la sixième ligne que la valeur de l'attribut `opened_zip` de `blue_denim` a été modifiée.

La septième ligne appelle la méthode `dye()` sur l'objet `blue_denim`. On voit bien grâce au `print` de la huitième ligne que le pantalon est devenu noir, et qu'il est à nouveau fermé. En effet, la méthode `close_zip()` est appelé directement à l'intérieur de la méthode `dye()`.

### Le paramètre `self`

Le paramètre `self` est un paramètre à ajouter dans la définition des méthodes de classes, pour accéder aux membres de la classe elle-même dans celle-ci.

Dans le constructeur `__init__()`, on utilise `self` pour définir les attributs de la classe. Dans les autres méthodes de la classe, `self` permet d'accéder à ces mêmes attributs.

Ainsi, les deux instructions ci-dessous font la même chose : appeler la méthode `open_zip` "sur" l'objet référencé par `blue_denim`.

```python
blue_denim.open_zip()
Trouser.open_zip(blue_denim)
```

## Exercices

- Ellen's Alien Game
- Phone Number
- Queen Attack
- Luhn
- Simple Cipher
- Matrix
- High Scores
- Grade School
- Connect Game
- Bank Account