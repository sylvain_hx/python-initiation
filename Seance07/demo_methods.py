class Trouser:

    def __init__(self, color, material):
        self.color = color
        self.material = material
        self.max_temperature = 40
        self.zip_opened = False

    def __str__(self):
        return (f"Trouser is {self.color}, made of {self.material} "
                f"and can be washed at {self.max_temperature} max.")

    def open_zip(self):
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False

    def dye(self, color):
        self.close_zip()
        self.max_temperature = 30
        self.color = color


def main():
    blue_denim = Trouser("blue", "denim")
    brown_chino = Trouser("brown", "chino")
    blue_denim.open_zip()  # Executes open_zip() method of object referenced by blue_denim variable
    print(f"is blue denim opened: {blue_denim.zip_opened}")
    print(f"is brown chino opened: {brown_chino.zip_opened}")
    blue_denim.dye("black")
    print(f"blue denim new color: {blue_denim.color}. Is it open ? {blue_denim.zip_opened}")
    print(blue_denim)
    print(brown_chino)


if __name__ == "__main__":
    main()
