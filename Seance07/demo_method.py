class Dog:

    def __init__(self, name, age, breed, color):
        self.name = name
        self.age = age
        self.breed = breed
        self.color = color

    def bark(self):
        print(f"{self.name} says waf")


def main():
    dog1 = Dog("jack", 2, "Fox Terrier", "orange")
    dog2 = Dog("jeff", 3, "Labrador", "yellow")
    print(f"{dog1.name}'s age: {dog1.age}, {dog1.name}'s color: {dog1.color}")
    print(f"{dog2.name}'s age: {dog2.age}, {dog2.name}'s color: {dog2.color}")
    dog1.colour = "white"
    print(dir(dog1))
    print(dir(dog2))
    dog1.bark()
    Dog.bark(dog1)
    dog2.bark()


if __name__ == '__main__':
    main()
