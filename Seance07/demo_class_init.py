class Dog:

    def __init__(self):
        self.age = None
        self.breed = "Generic"
        self.color = None


def main():
    jack = Dog()
    jeff = Dog()
    print(f"jack's age: {jack.age}, jack's color: {jack.color}")
    jack.age = 3
    print(f"jack's age: {jack.age}, jack's color: {jack.color}")
    jeff.color = "white"
    print(f"jeff's age: {jeff.age}, jeff's color: {jeff.color}")
    print(f"jack's age: {jack.age}, jack's color: {jack.color}")
    jeff.breed = "Labrador"
    jack.breed = "Fox Terrier"
    print(jeff.breed)
    print(jack.breed)


if __name__ == '__main__':
    main()
