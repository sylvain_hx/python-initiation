class Dog:
    pass


def main():
    jack = Dog()
    jeff = Dog()
    print(type(jack))
    print(id(jack))
    print(id(jeff))
    jack.age = 3
    print(jack.age)
    print(dir(jack))
    print(dir(jeff))


if __name__ == '__main__':
    main()