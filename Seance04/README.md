# Séance 4

## Le type `set`

Un set est une collection non ordonnée de longueur variable. On utilise les accolades `{}` pour créer un set :

```python
s = {1, 2, 3, 4, 5, 5, "Hello", 4.5}
```

Contrairement aux listes ou aux tuples, un set ne peut pas contenir deux fois un même élément. Dans l'exemple ci-dessus, `s` ne contient qu'une seule fois l'entier `5`, alors qu'il est bien noté deux fois entre les accolades `{}`.

Comme il s'agit d'une séquence non ordonnée, ses éléments ne sont pas indexables. La syntaxe `s[i]` "n'existe pas" pour des sets. Donc impossible de modifier un élément non plus. Mais on peut ajouter ou enlever des éléments.

### Les méthodes d'instance de `set`

Ci-dessous, les variables `s`, `s1` et `s2` référencent vers des `set`.

#### Les méthodes vérifiant des conditions

Ces méthodes renvoient `True` si la condition est vérifiée (même principe que pour certaines méthodes de `str`).

- `s1.isdisjoint(s2)` : si `s1` et `s2` sont disjoints, c.-à-d. s'ils n'ont aucun éléments en commun.
- `s1.issubset(s2)` : si `s1` est un sous-set de `s2`n c.-à-d. si tous les éléments de `s1` sont aussi dans `s2`.
- `s1.issuperset(s2)` : si `s1` est un sur-set de `s2`, c.-à-d. si tous les éléments de `s2` sont aussi dans `s1`. 

#### Les méthodes pour modifier un set

- `s.add(obj)` : ajoute `obj` dans le set.
- `s.clear()` : vide le set
- `s1.difference_update(s2)` : détermine la différence entre `s1` et `s2`. `s1` devient égal à celle-ci. La différence entre `s1` et `s2` ne contient que les éléments qui sont dans `s1` mais pas dans `s2`.
- `s.discard(obj)` : retire `obj` du set. Ne lève pas d'erreur si `obj` n'est pas dans le set.
- `s1.intersection_update(s2)` : détermine l'intersection de `s1` et `s2`. `s1` devient égal à celle-ci.
- `s.pop()` : retire au hasard un objet du set.
- `s.remove(obj)` : retire `obj` du set. Lève une erreur si `obj` n'est pas dans le set.
- `s1.symmetric_difference_update(s2)` : détermine la différence symétrique de `s1` et `s2`. `s1` devient égal à celle-ci. La différence symétrique entre `s1` et `s2` contient les éléments qui ne sont que dans un des deux sets, mais pas les deux en même temps.
- `s1.update(s2)` : ajoute les éléments de `s2` dans `s1`.

#### Les méthodes qui renvoient un nouveau set

- `s1.difference(s2)` : renvoie la différence.
- `s1.intersection(s2)` : renvoie l'intersection.
- `s1.symmetric_difference(s2)` : renvoie la différence symétrique.
- `s1.union(s2)` : renvoie l'union.

## Le type `frozenset`

Il est au set ce qu'est le tuple à la liste, c'est-à dire une collection non ordonnée (comme le set) et non modifiable (comme le tuple). La seule manière de créer un `frozenset` est d'appeler le constructeur de la classe :

```python
fs = frozenset([1, 2, 3, 4])
```

**NB** : Le type `set` est au type `list` ce que le type `frozenset` est au type `tuple`.

## Le type `dict`

Un dictionnaire est une collection non ordonnée et mutable. Qu'est-ce qui différencie donc un set d'un dictionnaire ?

Pour répondre à cette question, il suffit de faire l'analogie avec un dictionnaire papier. Un dictionnaire est une liste de mots, et à chacun de ces mots est rattaché une définition.

Dans un dictionnaire Python, chaque clef (*key* en anglais) du dictionnaire "possède" une valeur (*value* en anglais).

Un `dict` est donc une collection non ordonnée de paires d'objets clefs : valeurs.

Instantiation d'un `dict` :

```python
d = {
    6: 4.5,
    "size": [4, "flat"],
    75.23: False,
    (1, 2, 3): 10,
    }
```

NB : ici, l'instruction d'instantiation est écrite sur plusieurs lignes. Cela permet de rendre son code plus lisible, pour ne pas avoir toutes les paires sur une seule ligne (ce qui donnerait une ligne trop longue). On peut bien sûr utiliser ce formatage pour des listes, tuples...

N'importe quel objet peut être placé dans un dictionnaire. En revanche, la clef doit être un objet *hashable*. Pour faire simple, retenez qu'un objet *hashable* est immuable. Impossible par exemple donc d'utiliser une liste en tant que clef.

En pratique, ce sont très souvent des `str` qui sont utilisés comme clefs.

### Lecture, ajout, modification : utilisation des crochets 

Comme pour les listes (et les tuples), on peut utiliser des crochets pour lire ou écrire dans un dictionnaire :

```python
a = d[6]                # Gets value of the key 6
d[75.23] = 1            # Modify value of the key 75.23, since 75.23 is already present as a key in d
d["hello"] = 55         # Creates the pair "hello": 55, since "hello" is not yet a key in d
```

### Les méthodes d'instance de `dict`

Ci-dessous, la variable `d` référence vers un `dict`.

#### Les méthodes pour récupérer des éléments

- `d.get(key)` : renvoie la valeur associée à la clef `key`. Ne lève pas d'erreur si la `key` n'existe pas dans le dictionnaire, là où la syntaxe `d[key]` lèverait une erreur.
- `d.items()` : renvoie un itérable de tuples (clef, valeur) du dictionnaire.
- `d.keys()` : renvoie un itérable des clefs.
- `d.values()` : renvoie un itérable des valeurs.

#### Les méthodes pour ajouter ou modifier des éléments

- `d.setdefault(key, default_value)` : ajoute la paire `key`: `default_value` dans le dictionnaire, seulement si la `key` n'y est pas déjà présente. Le paramètre `default_value` a pour argument par défaut `None`. Dans les deux cas, la fonction renvoie la valeur rattachée à `key`.
- `d.update(pairs)` : ajoute les `pairs` dans le dictionnaire. En pratique `pairs` sera souvent un dictionnaire.

#### Les méthodes pour retirer des éléments

- `d.clear()` : retire tous les éléments du dictionnaire.
- `d.pop(key)` : retire la paire dont la clef est `key`.
- `d.popitem()` : retire la dernière paire ajoutée.

#### Les méthodes pour instancier un nouveau dictionnaire

- `d.copy()` : renvoie une copie du dictionnaire
- `dict.fromkeys(keys, value)` : renvoie un dictionnaire, ayant pour clefs le contenu de l'itérable `keys`. À toutes ces clefs sera associé la même valeur `value` (par défaut `None`.) **NB** : cette méthode est un constructeur. Rien n'empêche d'appeler cette méthode sur un objet de type `dict` (comme notre variable `d`), mais le dictionnaire renvoyé ne dépendra pas de ce dernier. Par conséquent, on appelle en général cette méthode non pas via une instance de  `dict`, mais via la classe `dict` elle-même.

## Exercices

- Inventory Management
- Cater Waiter
- Mecha Munch Management
- Locomotive Engineer
- Resistor Color Expert
- ETL
- Gigasecond
- Matching Brackets
- Atbash Cipher
- Diamond
- Protein Translation
- Diffie-Hellman
- Say
- Twelve Days
- Scrabble Score
- Proverb
