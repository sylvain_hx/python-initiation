d = {
    6: 4.5,
    "size": [4, "flat"],
    75.23: False,
    (1, 2, 3): 10,
    }

a = d[6]                # Gets value of the key 6
d[75.23] = 1            # Modify value of the key 75.23
d.update({75.23: 10})   # Modify a value of a key via update() method
d["hello"] = 55         # Create a new "hello" key and assigns it the value 55
d.update({75.23: 10})   # Modify a value of a key via update() method
d.update({"color": "red"})  # Create a new "color" key and assigns it the value "red"

d.pop("hello")  # Delete the item (key and value)
d.popitem()     # Delete the last added item. In this example "color": "red
del d[(1, 2, 3)]   # Delete via del Python keyword
# d.clear()       # Delete all items

d_keys = d.keys()       # Get the keys of the dict
d_values = d.values()   # Get the values of the dict
d_items = d.items()     # Get the keys and values of the dict

for key, value in d.items():
    print(f"{key} : {value}")
