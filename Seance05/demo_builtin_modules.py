import math
# from collections import * # Bad practice !
from datetime import date, datetime, time
import random as rand       # Import with an alias
import csv
import json


y = math.sqrt(100)
nb = rand.randint(5, 10)
print(nb)
d = datetime(day=31, month=1, year=2021, hour=12, minute=41, second=30)
print(d)

day = d.day        # day is an attribute of the object referenced by d. day will refer to its value, here 4
print(f"{d.minute}")
print(f"the day is {day}")

numbers = [1, 2, 4, 8]
print(sum(numbers))
print(4 ** 0.5)
math.pow(4, 0.5)
print(math.fsum(numbers))