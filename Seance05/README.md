# Séance 5

## Les modules

En Python, on appelle module un fichier Python. N'importe quel fichier que l'on écrit soi-même peut donc être utilisé en tant que module. Il existe également des modules *built-in*.

### Les modules *built-in*

TODO : présenter quelques modules utiles :

- random
- datetime
- time
- math
- collections
- itertools
- functools

```python
import math
import random as rand
from datetime import date
from collections import *   # Bad practice !
```

La première ligne sert à importer l'intégralité du module `math`. La deuxième importe également toutes les fonctions du module `time`. La troisième ligne importe uniquement la fonction `date` du module `datetime`.


NB : il vaut mieux éviter d'importer tout un module en faisant `from module import *`. En effet, si l'on utilise beaucoup de modules dans un programme, il peut y avoir des conflits de nommages, car certains modules peuvent avoir des fonctions avec le même nom. Privilégiez donc `import module`, afin d'écrire explicitement dans votre code le nom du module, suivi du nom de la fonction : `module.fonction()`.

Par convention, certains modules sont utilisés avec un alias.

Voici comment utiliser les fonctions de ces modules :

```python
y = math.sqrt(100)
j = uniform(10, 100)
d = date(day=4, month=12, year=2021)
print(d)
```

Comme pour les fonctions, compartimenter des fonctionnalités dans des modules permet de rendre son programme plus lisible, car mieux compartimenté, et de créer une "boite à outils" réutilisable dans d'autres projets.

### Utiliser ses propres modules

N'importe quel fichier Python peut être considéré comme un module, et donc importé par un autre script.

#### La condition `if __name__ == "__main__":`

Un fichier Python peut donc à la fois être considéré comme un module (importé dans un autre fichier) et comme un programme (et donc exécuté lui-même).

La condition `if __name__ == "__main__":` permet de définir clairement du code à exécuter uniquement si le fichier Python est exécuté en tant que script.

Ci-dessous, deux fichiers (*demo_main1.py* et *demo_main2.py*) pour exemple :

```python
def random_function():
    print("print inside the random function of demo_main1")


print("print outside the main of demo_main1")

if __name__ == "__main__":
    print("print inside the main of demo_main1")

if __name__ == "demo_main1":        # Condition will be true, and code will be executed if the file is not run as main
    print("demo_main1 run as module")
```

```python
import demo_main1

if __name__ == "__main__":
    demo_main1.random_function()
    print(__name__)                 # Will print __main__
    print(demo_main1.__name__)      # Will print demo_main1
```

Si vous lancez *demo_main1.py*, vous aurez la sortie console suivante :

```
print outside the main of demo_main1
print inside the main of demo_main1
```

Si maintenant vous lancez *demo_main2.py*, vous aurez la sortie suivante :

```
print outside the main of demo_main1
demo_main1 run as module
print inside the random function of demo_main1
__main__
demo_main1
```

Lorsque l'on exécute *demo_main1*, les deux messages sont affichés. Lorsqu'on importe *demo_main1* dans *demo_main2*, `print outside the main of demo_main1` est quand même affiché dans la console, mais pas `print inside the main of demo_main1`.

La condition `if __name__ == "__main__":` permet de "définir" du code qui ne sera exécuté uniquement si le fichier est exécuté directement.

#### La fonction main()

En Python, la bonne pratique conseille de définir dans son script une fonction `main()`, qui doit être la seule instruction placée dans le bloc `if __name__ == "__main__":`.

Ainsi, la structure d'un script doit être ainsi : 

```python
# Import are always done first, before any definition
# Order of imports statements is:
# built-in modules first, then external modules/librairies (ie numpy, pandas...) from pip, then custom modules
import math
import random as rand

import numpy as np

import demo_main1


# After import, there are functions and/or classes definitions
def some_function(a, b):
    c = a + b
    return c


def other_function(x, y, z):
    message = f"{x}, {y}, {z}"
    return message


# Last definition is the main() function
def main():
    result = some_function(3, 10)
    print(result)
    sentence = other_function("a", "b", "c")
    print(sentence)


# Finally, the name equal main condition, containing only the main() function call
if __name__ == "__main__":
    main()

```
