# Import are always done first, before any definition
# Order of imports statements is:
# built-in modules first, then external modules/librairies (ie numpy, pandas...) from pip, then custom modules
import math
import random as rand

import numpy as np

import demo_main1


# After imports, there are functions and/or classes definitions
def some_function(a, b):
    c = a + b
    return c


def other_function(x, y, z):
    message = f"{x}, {y}, {z}"
    return message


# Last definition is the main() function
def main():
    result = some_function(3, 10)
    print(result)
    sentence = other_function("a", "b", "c")
    print(sentence)


# Finally, the name equal main condition, containing only the main() function call
if __name__ == "__main__":
    main()
