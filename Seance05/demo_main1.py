def random_function():
    print("print inside the random function of demo_main1")


print("print outside the main of demo_main1")

if __name__ == "__main__":
    print("print inside the main of demo_main1")

if __name__ == "demo_main1":        # Condition will be true, and code will be executed if the file is not run as main
    print("demo_main1 run as module")
