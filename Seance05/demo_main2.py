import demo_main1

if __name__ == "__main__":
    demo_main1.random_function()
    print(__name__)                 # Will print __main__
    print(demo_main1.__name__)      # Will print demo_main1
