import pandas as pd
import mariadb


def main():
    with mariadb.connect(host="localhost", port=3306, user="root", password="isib") as connector:
        cursor = connector.cursor()
        connector.autocommit = True
        cursor.execute("DROP DATABASE IF EXISTS netflix_db;")
        cursor.execute("CREATE DATABASE IF NOT EXISTS netflix_db;")
        cursor.execute("CREATE TABLE IF NOT EXISTS netflix_db.shows ("
                       "show_id int PRIMARY KEY,"
                       "show_type varchar(12) NOT NULL,"
                       "title varchar(255) NOT NULL"
                       ");")
        netflix_df = pd.read_csv("netflix_titles.csv")
        shows_df = netflix_df[["show_id", "type", "title"]]
        print(shows_df.head(5))
        shows_df["show_id"] = pd.Series([int(show_id[1:]) for show_id in shows_df["show_id"]])
        print(shows_df.head(5))
        # Insert the first line of the dataframe into database table
        """
        query = (f"INSERT INTO netflix_db.shows VALUES ({shows_df['show_id'][0]}, "
                 f"'{shows_df['type'][0]}', "
                 f"'{shows_df['title'][0]}');")
        print(query)
        cursor.execute(query)
        """
        # Parse dataframe into list of lists
        data = shows_df.to_numpy().tolist()
        cursor.executemany(f"INSERT INTO netflix_db.shows VALUES (?, ?, ?)", data)

        cursor.execute("CREATE TABLE IF NOT EXISTS netflix_db.show_types"
                       "(type_ID int PRIMARY KEY,"
                       "name varchar(12) UNIQUE NOT NULL);")

        show_types = netflix_df["type"].drop_duplicates()
        show_types.index = [index_number+1 for index_number in list(show_types.index)]
        print(show_types)
        data = [(index, show_type) for index, show_type in zip(show_types.index, show_types)]
        print(data)
        cursor.executemany("INSERT INTO netflix_db.show_types VALUES (?, ?)", data)

        cursor.execute("ALTER TABLE netflix_db.shows "
                       "ADD FOREIGN KEY (show_type) REFERENCES netflix_db.show_types(name)")
        cursor.execute("INSERT INTO netflix_db.show_types VALUES "
                       "(3, 'Documentary')")
        cursor.execute("INSERT INTO netflix_db.shows VALUES "
                       "(8808, 'Documentary', 'Wild Animals');")


if __name__ == '__main__':
    main()
