import mariadb

# Example using context manager (with keyword)
with mariadb.connect(host="127.0.0.1", port=3306, user="root", password="isib") as connector:
    cursor = connector.cursor()
    cursor.execute("insert into isib.students values "
                   "(24531, 'Doe', 'John'), "
                   "(49532, 'Smith', 'Joe'), "
                   "(63254, 'Keller', 'Alice');")
    connector.commit()

# Other way to create a connector securely, via a try except statement
try:
    connector = mariadb.connect(host="127.0.0.1", port=3306, user="root", password="isib")
    connector.autocommit = True
    cursor = connector.cursor()
except mariadb.Error as e:
    print(f"Error connecting to mariadb server : {e}")

# Same for executing queries and catch potential errors during process
try:
    cursor.execute("insert into isib.students values "
                   "(14725, 'Big', 'Jack'), "
                   "(55789, 'Small', 'John'), "
                   "(45231, 'Keller', 'Adam');")
except mariadb.Error as e:
    print(f"Error executing mariadb query : {e}")

connector.close()

# A simple way to retrieve data from a select query
with mariadb.connect(host="127.0.0.1", port=3306, user="root", password="isib") as connector:
    cursor = connector.cursor()
    cursor.execute("select * from isib.students;")
    connector.commit()
    data = cursor.fetchall()
    print(data)
    print(type(data))
    print(data[0][1])
    data_bis = cursor.fetchone()
    print(data_bis)

with mariadb.connect(host="127.0.0.1", port=3306, user="root", password="isib") as connector:
    cursor = connector.cursor()
    cursor.execute("select * from isib.students;")
    connector.commit()
    data1 = cursor.fetchone()
    print(data1)
    data2 = cursor.fetchone()
    print(data2)
    data3 = cursor.fetchmany(4)
    print(data3)
