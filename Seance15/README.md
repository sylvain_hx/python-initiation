# Séance 15

Dans cette séance, nous allons voir comment utiliser la librairie `matplotlib`, utilisée pour tracer des graphiques.

## matplotlib

Comme pour NumPy et pandas, il faut installer matplotlib pour pouvoir l'importer et l'utiliser :

```python
import matplotlib.pyplot as plt
```

Elle permet de créer très simplement des graphiques :

```python
y = np.array([2, 4, 6, 8, 10])
plt.plot(y)
plt.ylabel("array y")
plt.show()
```

Ce code va afficher ce qu'on appelle une figure. Le graphique présenté affiche l'*array* `y` en ordonnée, avec les valeurs en abscisse par défaut (pas de 1). Si l'on veut spécifier un autre pas en abscisse, il faut passer une deuxième séquence en argument :

```python
y = np.array([2, 4, 6, 8, 10])
x = np.array([10, 20, 50, 100, 250])
plt.plot(x, y)
plt.ylabel("array y")
plt.xlabel("array x")
plt.show()
```

Il est possible de fixer la taille des axes :

```python
y = np.array([2, 4, 6, 8, 10])
x = x = np.array([10, 20, 50, 100, 250])
plt.plot(x, y)
plt.ylabel("array y")
plt.xlabel("array x")
plt.axis([0, 300, 0, 12])
plt.show()
```

Il est également possible de tracer plusieurs *arrays* sur un même graphique :

```python
y = np.array([2, 4, 6, 8, 10])
y2 = np.array([4, 8, 12, 16, 20])
y3 = np.random.randint(0, 10, 5)
x = x = np.array([10, 20, 50, 100, 250])
plt.plot(x, y, "xg", x, y2, "sr", x, y3, "b--")        # x : show crosses for points, g : green color
plt.ylabel("arrays y")
plt.xlabel("array x")
plt.axis([0, 300, 0, 25])
plt.show()
```

Matplotlib permet de facilement tracer des données contenues dans un dictionnaire ou une *data frame* via les clefs et labels. Exemple :

```python
dico = {"item n°": (21, 45, 32, 75, 62), "quantity sold": (4, 8, 3, 7, 4)}
df = pd.DataFrame(dico)
plt.bar("item n°", "quantity sold", data=df)
plt.ylabel("quantity sold")
plt.xlabel("item n°")
plt.show()
```

Il est également possible de tracer plusieurs graphiques en une seule figure :

```python
plt.figure(1)
plt.subplot(221)
plt.title("this is plot 1 of fig 1")
plt.plot(x, y)
plt.subplot(222)
plt.title("this is plot 2 of fig 1")
plt.bar("item n°", "quantity sold", data=d)
#plt.axis([20, 80, 0, 40])
plt.subplot(223)
# what I want to plot on case 3
plt.subplot(2, 2, 4)
plt.suptitle("Two graphs in one figure + 2 empty graphs")
plt.show()
```

Et on peut même créer plusieurs figures avec un seul programme :

```python
plt.figure(1)
plt.subplot(221)
plt.title("this is plot 1 of fig 1")
plt.plot(x, y)
plt.subplot(222)
plt.title("this is plot 2 of fig 1")
plt.bar("item n°", "quantity sold", data=d)
#plt.axis([20, 80, 0, 40])
plt.subplot(223)
# what I want to plot on case 3
plt.subplot(2, 2, 4)
plt.suptitle("Two graphs in one figure + 2 empty graphs")
# plt.show()

plt.figure(2)
plt.scatter([1, 2, 3, 4, 5], y3)
plt.title("Here comes fig 2")
plt.show()
```

Il est possible de réaliser des figures et des graphiques via une approche d'avantage orientée objet :

```python
fig1 = plt.figure()
ax1 = fig1.add_subplot(2, 1, 1)
ax1.plot(x, y)
ax2 = fig1.add_subplot(2, 1, 2)
ax2.plot(x, y2)
ax2.annotate("arrow annotation" , xy=(100, 16), xytext=(150, 10), arrowprops=dict(facecolor="red", shrink=0.05))
plt.show()
```

Il est également possible de sauvegarder une figure dans un fichier image :

```python
fig1.savefig("figure1.png")
```

## Exercices

### Covid

Source : https://www.ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide

1. Tracez un graphique représentant l'évolution des nouveaux cas entre le 15 mars et le 15 juin 2020 pour la France et l'Espagne
2. Tracez un diagramme circulaire (fonction `pie()`), les cas de la Belgique, des Pays-Bas et du Luxembourg par rapport au total des cas de ces trois pays, sur la période du 1er juillet au 1er décembre 2020.

### Électricité mondiale

Source : https://www.kaggle.com/datasets/akhiljethwa/global-electricity-statistics

1. Tracez sur un même graphique, l'évolution de la consommation nette de la Belgique (en jaune), des Pays-Bas (en orange) et du Danemark (en rouge). Le graphique doit contenir une légende indiquant pour quelle courbe correspond à quel pays.
2. Créez une figure de 4 graphiques : production nette, consommation nette, import et export de la France, de 1980 à 2021.
3. Tracez un diagramme en barres de la capacité installée, uniquement pour les pays américains (nord et sud) en 2019.
4. Tracez un nuage de points de tous les pays européens, avec en abscisse les imports et en ordonnée les exports.
