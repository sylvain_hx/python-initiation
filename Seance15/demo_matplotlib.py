import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def plot1():
    y = np.array([2, 4, 6, 8, 10])
    y2 = np.array([4, 8, 12, 16, 20])
    y3 = np.random.randint(0, 10, 5)
    x = np.array([10, 20, 50, 100, 200])
    x2 = np.array([0, 50, 100, 120, 150])

    plt.plot(x, y, "xg", x, y2, "sr", x2, y3, "b--")        # x : show crosses for points, g : green color
    plt.ylabel("arrays y")
    plt.xlabel("array x")
    plt.axis((0.0, 300.0, 0.0, 25.0))
    plt.title("3 functions on the same plot")
    plt.show()


def plot2():
    d = {"item n°": (21, 32, 45, 75, 62), "quantity sold": (4, 8, 3, 7, 15)}
    df = pd.DataFrame(d)
    print(df)

    plt.bar("item n°", "quantity sold", data=df)
    plt.ylabel("quantity sold")
    plt.xlabel("item n°")
    plt.show()


def plot_subplot():
    x = np.array([10, 20, 50, 100, 200])
    y = np.array([2, 4, 6, 8, 10])
    d = {"item n°": (21, 32, 45, 75, 62), "quantity sold": (4, 8, 3, 7, 4)}
    plt.figure(1)
    plt.subplot(221)
    plt.title("this is plot 1 of fig 1")
    plt.plot(x, y)
    plt.subplot(222)
    plt.title("this is plot 2 of fig 1")
    plt.bar("item n°", "quantity sold", data=d)
    plt.subplot(223)
    plt.plot((1, 2, 3), (50, 100, 200))
    # what I want to plot on case 3
    plt.subplot(2, 2, 4)
    plt.suptitle("Two graphs in one figure + 2 empty graphs")
    plt.show()

def plot_2figs():
    x = np.array([10, 20, 50, 100, 200])
    y = np.array([2, 4, 6, 8, 10])
    d = {"item n°": (21, 32, 45, 75, 62), "quantity sold": (4, 8, 3, 7, 4)}
    df = pd.DataFrame(d)
    plt.figure(1)
    plt.subplot(221)
    plt.title("this is plot 1 of fig 1")
    plt.plot(x, y)
    plt.subplot(222)
    plt.title("this is plot 2 of fig 1")
    plt.bar("item n°", "quantity sold", data=df)
    # plt.axis([20, 80, 0, 40])
    plt.subplot(223)
    # what I want to plot on case 3
    plt.subplot(2, 2, 4)
    plt.suptitle("Two graphs in one figure + 2 empty graphs")
    plt.show()

    plt.figure(2)
    plt.scatter([1, 2, 3, 4, 5], y)
    plt.title("Here comes fig 2")
    plt.show()


def plot_scatter():
    y3 = np.random.randint(0, 10, 5)
    plt.figure(2)
    plt.scatter([1, 2, 3, 4, 5], y3)
    plt.title("Here comes fig 2")
    plt.show()


def pythonic_plot():
    y = np.array([2, 4, 6, 8, 10])
    y2 = np.array([4, 8, 12, 16, 20])
    x = np.array([10, 20, 50, 100, 200])
    fig1 = plt.figure(1)
    subplot_1 = fig1.add_subplot(2, 1, 1)
    subplot_1.plot(x, y)
    subplot_2 = fig1.add_subplot(2, 1, 2)
    subplot_2.plot(x, y2)
    subplot_2.annotate("arrow annotation", xy=(100, 16), xytext=(150, 10), arrowprops=dict(facecolor="red", shrink=12))
    plt.show()
    fig1.savefig("figure1.png")


def main():
    # plot1()
    # plot2()
    # plot_subplot()
    # plot_2figs()
    # plot_scatter()
    pythonic_plot()


if __name__ == "__main__":
    main()
