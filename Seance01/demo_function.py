def print_hello():
    print("hello")


def add_two_numbers(a, b):
    return a + b


def power(x, y=2):
    return x**y


def count_number_of_a(message: str):
    count = 0
    for char in message:
        if char == "a":
            count += 1
    return count


def guess_a_number():
    print("You have 10 tries to guess the secret number")
    tries = 10
    guess = int(input("input your first guess : "))
    while tries > 0:
        if guess == 13:
            print("Well played, you guessed the number !")
            return
        else:
            tries -= 1
            if tries == 0:
                print("You lost")
                return
            else:
                guess = int(input("Only " + str(tries) + " tries left. Enter a new guess : "))


def sort_vegetables():  # An example of a concrete usage of `pass`
    pass


def main():
    print_hello()
    sum1 = add_two_numbers(2, 4)
    print(sum1)

    p1 = power(2)
    p2 = power(4, 4)
    p3 = power(y=3, x=4)
    print(p1)
    print(p2)
    print(p3)
    nb_of_a = count_number_of_a("I have a certain quantity of a")
    print(nb_of_a)
    guess_a_number()


main()
