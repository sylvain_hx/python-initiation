# Séance 1

## Introduction : Qu'est-ce que Python ?

Le terme Python peut tout autant servir à désigner un langage de programmation qu'un logiciel.

### Un langage de programmation

- C'est un langage de programmation inventé en 1989/90, à l'origine pour faire du *scripting* (Le terme script désigne habituellement un programme servant à automatiser des tâches 'répétitives').

- C'est un langage sous licence *open-source*. Cela signifie qu'il est utilisable gratuitement, y compris à des fins commerciales, et que le "code source" du projet est consultable sur Internet et réutilisable.

- C'est un langage interprété. Cela signifie qu'à chaque nouvelle exécution d'un programme/script Python, l'interpréteur Python va exécuter les instructions écrites dans ledit script. C'est-à-dire les "traduire" une par une pour que l'ordinateur les comprenne et puisse faire ce qui est demandé par le code Python. On trouve "à l'inverse" les langages compilés (comme le C/C++) ou le compilateur va "analyser" l'intégralité d'un code source pour donner une version traduite de ce dernier, compréhensible par la machine. Voici une analogie pour résumer :
	- Langage interprété : vous discutez avec une personne qui parle une langue que vous ne comprenez pas. Vous êtes accompagné d'un·e interprète (le mot est d'ailleurs bien choisi) qui vous traduit ses paroles "en direct", phrase par phrase.
	- Langage compilé : vous discutez avec cette même personne. La personne note tout ce qu'elle veut vous dire d'un coup, dans son application Google Translate. Vous récupérez son téléphone, sur lequel vous pouvez lire la traduction de tout ce qu'elle souhaitait vous dire.
        - Avantage : la portabilité. Cela signifie que le programme pourra fonctionner sur n'importe quelle machine sur laquelle est installé un interpréteur Python.
        - Inconvénient : la performance. Même si celle-ci est continuellement améliorée, une même tâche sera exécutée plus rapidement en langage compilé (typiquement, en C++) qu'en Python.

- C'est un langage orienté objet. La programmation orientée objet est un paradigme - une "vision" - de programmation. Un objet est une "entité" qui représente quelque chose. Un objet peut posséder des caractéristiques et des fonctionnalités. En Python, **tout est objet**.

### Un logiciel

Pour pouvoir exécuter du code Python sur sa machine, il faut installer un interpréteur Python. Il est téléchargeable sur le site officiel : https://www.python.org/downloads/

- Si vous êtes sur Windows, il faut télécharger l'exécutable et installer Python, comme n'importe quel autre logiciel.
- Si vous êtes sur MacOS, Python est peut-être déjà installé sur votre machine. Mais il y a des chances que cela soit une version trop ancienne. Il vaut donc mieux télécharger et installer la version la plus récente.
- Si vous êtes sur Linux, Python est forcément installé sur votre machine, et dans une version (plus ou moins) récente, en fonction de la distribution.

## Exécuter son premier script Python

### Installer un éditeur de code

Avoir un intepréteur Python installé suffit pour exécuter des scripts sur sa machine. N'importe quel éditeur de fichiers texte (donc même le très baisque bloc-notes Windows) peut être utilisé pour lire, créer ou modifier un script Python.

Évidemment, le bloc-notes Windows n'est pas un outli fait pour éditer du code. Il est beaucoup plus judicieux d'utiliser ce qu'on appelle un éditeur de code. Par exemple :

- Windows : Notepad++ : https://notepad-plus-plus.org/downloads/
- MacOS et Linux : Kate, Geany,...

Qu'apporte un éditeur de code par rapport à un simple éditeur de texte comme le bloc-notes ?
- Détection du langage
- Coloration syntaxique 
- Aide à la frappe

Dans quels cas utiliser un éditeur de code plutôt qu'un IDE complet ?
- Plus léger, donc plus rapide à lancer. Utile si l'on souhaite lire ou modifier un fichier rapidement
- Plus polyvalent : pourra ouvrir et reconnaitre n'importe quel type de fichier code, là où les IDEs sont en général plus spécifiques.

### Écrire le script

Commençons par lancer Notepad++. Une zone de texte apparait. Écrivons la ligne suivante :

```
print("hello world")
```

Vous venez d'écrire votre première instruction en Python. En informatique, une instruction est une étape du programme. La plupart du temps, une instruction correspond à une ligne de code.

`print()` est ce qu'on appelle une fonction. Nous verrons ce qu'est une fonction en programmation plus tard. Ici, il suffit de savoir que cette fonction sert à afficher un message en sortie.

Pour l'instant, le code n'est pas coloré, car Notepad++ "ne sait pas" que le code écrit est en Python. Enregistrons maintenant ce code dans un fichier (Via "Enregistrer sous"). Appelons ce fichier "hello_world".

**NB** : Attention, sur Windows, l'extension d'un fichier n'apparait pas par défaut dans son nom. Lors de l'enregistrement du fichier, indiquer simplement "hello_world" dans le champ du nom, et sélectionner le type ".py". Sur MacOS ou Linux, l'extension apparait systématiquement, et peut donc être directement écrite à la fin du nom (donc "hello_world.py").

Une fois le fichier enregistré (dans un dossier quelconque, par exemple le bureau), nous allons pouvoir l'exécuter.

### Exécuter le script

- Windows : Lancer l'invite de commande ("cmd") ou Windows Powershell. Il s'agit d'une invite de commande reprenant les commandes UNIX (qu'on utilise sur Linux ou MacOS)
- MacOs : lancer le terminal
- Linux : lancer le terminal de votre distribution (Konsole, Terminal...)

Dans un terminal UNIX (ou Windows Powershell) : 

- La commande `cd` (pour "Change Directory", changer de dossier) permet de changer de dossier courant. C'est l'équivalent de l'explorateur de fichier qu'on utilise communément, mais en ligne de commande. Ici, entrez `cd /Le/chemin/vers/le/dossier/où/se/trouve/le/script/hello_world/`.
- La commande `python hello_world.py` exécutera le script que vous venez d'écrire.

Félicitations, vous venez d'exécuter votre premier programme Python.

## Le *REPL* : le *Shell* interactif de Python

Entrez simplement `python` dans un terminal. Comme aucun fichier n'est spécifié, Python va lancer la console interactive, ou REPL en anglais. Le *REPL* permet d'interpréter "en direct" des instructions Python.

Le *REPL* est un outil utile. Il permet de tester rapidement des petits bouts de code, sans avoir à écrire dans un fichier "en dur".

## Les opérateurs mathématiques

Dans le *REPL*, écrivez l'instruction suivante, puis appuyez sur entrée :

```
4+5
```

Le symbole `+` est un opérateur (comme en mathématiques). En Python, comme dans la plupart des langages, nous pouvons utiliser des opérateurs pour faire des calculs mathématiques. En voici la liste :

| symbole | fonctionnalité   |
|---------|------------------|
| \+      | addition         |
| \-      | soustraction     |
| \*      | multiplication   |
| \**     | puissance        |
| \/      | division         |
| \//     | division entière |
| \%      | modulo           |

Effectuer des calculs "en direct" peut sembler intéressant, mais le vrai intérêt va réellement être de pouvoir stocker ces résultats, afin de pouvoir les réutiliser plus tard dans notre script.

## Les variables

En informatique, une variable sert à contenir une valeur, et plus particulièrement en Python, à référencer vers un objet.

Rappel : en Python **tout est objet**.

Prenons l'exemple d'une bibliothèque universitaire. Si on souhaite trouver un livre, on a besoin de sa référence, ou plus exactement de sa côte de rangement (par exemple : BIB. : R. des Goujons 53 EX/COR KAN). Cette côte, c'est l'équivalent de notre variable. Elle me permet de savoir où se trouve un livre donné, mais **N'EST PAS** le livre en lui-même. Le livre, ce qui est référencé par la côte, c'est l'équivalent de l'objet.

De la même manière, on peut dire que les étagères de la bibliothèque sont l'équivalent de la mémoire vive (la RAM) de la machine. Quand un nouvel objet est créé, celui-ci est stocké dans la RAM. Et lors de l'exécution d'un programme, c'est la variable qui permet de savoir où est rangé tel objet dans la mémoire.

Écrivez et exécutez cette instruction dans le *REPL* :

```python
x = 5
```

`x` est ce qu'on appelle une variable. La première instruction affecte la valeur `5` à la variable `x`. Contrairement aux mathématiques, le symbole `=` ne symbolise pas l'égalité entre deux choses, mais bien le fait de venir affecter l'objet à sa droite à la variable à sa gauche (à gauche du `=` se trouvera donc toujours une variable).

**Convention de nommage** : En Python, on utilise le format *snake_case* : tous les mots en minuscule, en les séparant par un `_`. Exemple : `length`, `nb_values`. Également, il est fortement conseillé d'écrire son code en anglais. Et surtout, éviter les caractères autres qu'alphanumériques (pas d'accents, de cédilles...).

Ci-dessous un lien reprenant toutes les conventions de nommage du langage : 
https://pythonguides.com/python-naming-conventions/

Dans le *REPL*, il suffit de rentrer une variable pour afficher ce vers quoi elle référence. Essayez en entrant `x`.

Vous pouvez maintenant entrer les instructions suivantes : 

```
y = 10
z = x + y
z
```

Ici, on voit bien que `z` "vaut" `15`. Mais ce n'est pas réellement correct de dire que `z` vaut `15`, car `z` est une variable qui référence vers un objet de **type** `int`, ici un entier qui vaut 15.

## Les types natifs

**NB** : À partir de maintenant, nous allons travailler en écrivant dans un script puis en l'exécutant, et plus dans le *REPL*.

En python, chaque objet a un type. C'est-à-dire ce qu'il \*est\*. Il est tout à fait possible (c'est le but de la programmation orientée objet) de créer ses propres types. Mais Python fournit déjà des types dits natifs (*builtin* en anglais), qui sont utilisables directement dans n'importe quel script.

Nous avons déjà parlé du type `int`, qui sert à représenter des nombres entiers. Voici quelques un des types natifs les plus utilisés :

| Dénomination en Python | en français                                                 |
|------------------------|-------------------------------------------------------------|
| `int`                  | nombre entier optimisé                                      |
| `float`                | nombre à virgule flottante                                  |
| `complex`              | nombre complexe                                             |
| `bool`                 | booléen                                                     |
| `str`                  | chaîne de caractère                                         |
| `list`                 | liste ordonnée de longueur variable                         |
| `tuple`                | liste ordonnée de longueur fixe                             |
| `set`                  | liste non ordonnée de longueur variable                     |
| `frozenset`            | liste non ordonnée de longueur fixe                         |
| `dict`                 | dictionnaire : liste non ordonnée de paires *key* : *value* |
| `bytes`                | liste ordonnée d'octets de longueur fixe                    |
| `bytearray`            | liste ordonnée d'octets de longueur variable                |
| `function`             | fonction                                                    |
| `file`                 | fichier                                                     |
| `module`               | module                                                      |
| `NoneType`             | absence de type                                             |


Certains de ces types seront abordés plus tard. Commençons par présenter les types "de base".

### Les types de base

`int` : nombre entier. Rien de compliqué ici, représente un nombre entier, positif ou négatif.

`float` : nombre à virgule flottante. Idem, pas besoin de savoir ce que signifie flottante pour les utiliser. À noter qu'en Python (et dans la plupart des langages), on représente la virgule par un point : `a = 4.52`

`bool` : un booléen ne peut prendre que deux valeurs : `True` ou `False`. Nous verrons à quoi ce type sert un peu plus tard.

`str` : *String*, chaine de caractère. À vrai dire, nous avons déjà utilisé une chaine de caractère :

```python
str1 = "hello world"
```

Ici, `"hello world"` en est une. Pour écrire une chaîne de caractère, on note celle-ci entre guillemets. On peut aussi utiliser des apostrophes ou même des triples guillemets.

```python
str1 = "hello"
str2 = "hello \"friends"
str3 = """I hope you are doing "well"\n me, yes"""
print(str2 + str3)
```

Deux choses à noter : il existe des caractères "spéciaux", que l'on peut écrire dans une chaîne à l'aide du `\` (antislash ou *backslash* en anglais). Par exemple, `\n` est un caractère de saut de ligne. Un autre caractère souvent utilisé est `\t`, pour insérer une tabulation.

Dans la troisième ligne, nous utilisons le symbole `+` pour afficher les deux chaînes de caractères à la suite. On appelle cela une concaténation.

Une chaîne de caractère est une **séquence**. Cela signifie que l'on peut voir celle-ci comme une "suite" de caractères simples. Essayez d'exécuter ces instructions dans votre script :

```python
str1 = "hello world"
first_char = str1[0]
third_char = str1[2]
last_char = str1[-1]
print(first_char)
print(third_char)
print(last_char)
```

Sera alors affiché dans la console : 

```
h
l
d
```

Les instructions lignes 2, 3 et 4 viennent à chaque fois affecter un des caractères de la chaîne dans 3 variables différentes.

On appelle indice (souvent appelé `i`) l'entier placé entre crochets, permettant de récupérer le i-ème élément de la chaîne.

**IMPORTANT : en informatique, on commence toujours à compter à partir de 0.**

Utiliser un indice négatif permet de démarrer depuis la fin de la chaîne. (-2 renverra l'avant-dernier caractère, et ainsi de suite).

On dit que Python qu'il est un langage dynamique. Étudions ce bout de code :

```python
a = 5
a = 8.6
a = "Sylvain"
```

Ici, nous n'avons créé qu'une seule variable. On lui affecte d'abord un `int`, puis un `float` et enfin un `str`. Si vous ajoutez ce code à votre script et que vous l'exécutez, tout va fonctionner correctement. Cette caractéristique rend Python assez flexible d'utilisation. Dans d'autres langages, il faut spécifier explicitement le type d'une variable, et il serait donc impossible pour celle-ci de successivement faire référence à un entier puis à un *string*.

### Le *casting* : la conversion de type

Le code ci-dessous présente comment obtenir un `str` à partir d'un `int` ou d'un `float`, et inversement :

```python
nb = 5
nb_str = str(nb)
txt1 = "4"
txt1_int = int(txt1)
txt2 = "5.23"
txt2_float = float(txt2)
```

`nb` est de type `int`. La ligne en dessous de cette affection sert à convertir cet entier en une chaîne de caractère, qui sera `"5"`. Cette conversion s'effectue grâce à la fonction `str()`.

Inversement, il est possible de convertir une chaîne de composée de symboles représentant un nombre (entier ou à virgule) en un type `int` ou `float`. Pour cela, on utilise les fonctions `int()` et `float()`.

On dit que ces trois fonctions "renvoient" un objet. En effet, celles-ci sont placées à gauche de l'opérateur d'affectation (le `=`). `nb`, `a` et `b` vont donc respectivement référencer vers un objet de type `str`, `int` et `float`.

Vous souhaitez connaître le type d'un objet ? Utilisez la fonction `type()` :

```python
print(type(nb))
print(type(txt1_int))
print(type(txt2_float))
```

### Les *f-strings*

Convertir un nombre en un type `str` peut être utile, et, pour l'instant, peut vous sembler la seule solution pour afficher un nombre et des *strings* dans un même `print()`. Depuis la version 3.6, Python contient un type de *string* particulier : les *f-string* pour *Formatted string*. Prenons un exemple :

```python
english_mark = 18
math_mark = 17.5
```

On souhaite afficher dans la console le message suivant :

```
You got 18 in english and 17.5 in maths.
```

On peut tout à fait utiliser la concaténation via `+` :

```python
print("You got " + str(english_mark)
      + " in english " + str(math_mark) + "  in maths.")
```

Mais l'instruction est peu lisible. Même résultat mais en utilisant une *f-string* : :

```python
print(f"You got {english_mark} in english and {math_mark} in maths.")
```

Le `f` devant les guillemets indiquent qu'il s'agit d'une *f-string*. S'il n'est pas présent, les accolades `{` et `}` seront des caractères comme les autres à l'intérieur de la chaîne.

## La logique booléenne

Nous avons évoqué précédemment le type `bool`, qui peut prendre deux valeurs : `True` ou `False`. On peut ainsi faire référencer une variable vers un booléen

```python
a, b = True, False
```

**NB** : cette instruction est une affectation parallèle : `a` devient égal à `True`, et `b` à `False`.

### Les opérateurs de comparaison

Utiliser une variable pouvant être `True` ou `False` peut présenter un intérêt. Mais l'intérêt principal des booléens est de pouvoir indiquer le résultat d'une **comparaison**, qui s'effectuent à l'aide de comparateurs :

| symbole      | signification                            |
|--------------|------------------------------------------|
| `<`          | strictement inférieur                    |
| `>`          | strictement supérieur                    |
| `<=`         | inférieur ou égal                        |
| `>=`         | supérieur ou égal                        |
| `==`         | égal                                     |
| `!=`         | différent                                |
| `x is y`     | x et y représentent le même objet        |
| `x is not y` | x et y ne représentent pas le même objet |

Testez maintenant ces lignes de code dans votre script :

```python
a = 5 < 6
print(a)
```

La console affiche `True`.

### Les opérateurs logiques

Les opérateurs de comparaison se retrouvent souvent utilisés avec les opérateurs logiques :

| symbole   | signification |
|-----------|---------------|
| `x or y`  | OU booléen    |
| `x and y` | ET booléen    |
| `not x`   | NON booléen   |

Ajoutez les lignes suivantes à votre script : 

```python
a = 5 < 6 and 10 < 6
print(a)
```

Cela va afficher `False`. En effet, on comprend intuitivement que ET signifie que les deux *conditions* (celle à gauche et celle à droite du `and`) doivent être vraies.

Si vous remplacez le `and` par un `or`, alors le `print()` affichera cette fois `True`. Car il suffit qu'une des deux conditions soient vraies pour que l'ensemble renvoie `True`.

### Le *casting* vers un booléen

Il est possible de convertir des objet en booléen, grâce à la fonction `bool()` : 

```python
bool(1)     # Return True
bool(0)     # Return False
bool(-2)    # Return True
bool(0.5)   # Return True
bool(0.0)   # Return False
bool("Hi")  # Return True
bool("")    # Return False
bool(" ")   #Return True
```

N'importe quel nombre (donc même négatif) est équivalent à `True`. Seul 0 est équivalent à `False`. Une chaîne vide est équivalent à `False`. À partir du moment où elle contient au moins un caractère (même si c'est un caractère blanc comme un espace ou un saut à la ligne), elle est équivalente à `True`.

## Les commentaires

En python, les commentaires commencent par le caractère dièse : `#`. En informatique, on appelle commentaire du texte "invisible" pour l'interpréteur. Comme leur nom l'indique, ils servent à commenter son code, pour soi-même, ou pour les autres.

**NB** : comment commenter simplement plusieurs lignes :

```python
"""
print(4+5)
print(5+10)
print(10+50)
"""
```

## Les erreurs

Ajoutez cette ligne à votre script :

```python
print(4		# Will generate an error
```

Puis relancez votre programme. Vous devriez obtenir un affichage similaire dans la console : 

```
File "/home/sylvain/Gits/python-initiation/Seance01/demo1.py", line 54
    print(4
         ^
SyntaxError: '(' was never closed

Process finished with exit code 1
```

Votre programme a généré une erreur. En effet, la parenthèse de la fonction `print()` n'a pas été fermé.

Il est important de lire le message d'erreur. Les informations qu'il fournit peuvent souvent aider à détecter le problème dans son code.

## Les fonctions

Nous avons déjà utilisé plusieurs fonctions de base du langage Python. Dans cette partie, nous allons voir comment écrire nos propres fonctions.

Une fonction informatique peut recevoir quelque chose en entrée et peut renvoyer quelque chose en sortie.

Par exemple, la fonction `input()` prend en argument d'entrée une chaîne de caractère pour l'afficher dans la console et renvoie la chaîne de caractère rentrée dans la console par l'utilisateur·ice.

Une fonction peut ne prendre aucun argument en entrée. Également, une fonction peut ne "rien" renvoyer. En Python, si une fonction "ne renvoie rien", elle renvoie en pratique un objet de type `None`.

Voici la syntaxe pour écrire une fonction :

```python
def print_hello():
    print("hello")
```

Le mot-clef `def` permet de marquer la définition de la fonction : ici `print_hello` sera le nom de la fonction.

Lorsqu'on définit une fonction, on doit définir le code source de cette dernière dans ce qu'on appelle un bloc de code. En Python, le symbole `:` permet de définir le début d'un bloc.

Contrairement à d'autres langages (comme le C/C++), ce qui fait qu'une instruction fait partie d'un bloc ou non est son indentation (Comment est-elle décalée vers la droite). Pour indenter d'un niveau, on utilise la touche tabulation du clavier.

La fonction ci-dessus ne possède aucun paramètre d'entrée, et "ne renvoie rien" (donc en réalité `None`).

**NB** : La convention de nommage pour les fonctions est la même que pour les variables (car le nom d'une fonction n'est rien d'autre qu'une variable) : tout en minuscules, avec des `_` pour séparer les mots.

Essayez maintenant d'exécuter la fonction que vous venez d'écrire :

```python
print_hello()
```

La console affiche bien `hello`.

### Les paramètres, les arguments, le renvoi

Écrivons maintenant une fonction effectuant une addition de deux nombres et qui renvoie le résultat :

```python
def add_two_numbers(a, b):
    return a + b
```

Cette fonction possède deux **paramètres** : `a` et `b`. Lorsqu'elle sera appelée, elle renverra la somme de ces deux objets via le mot clef `return`.

Testons cette fonction :

```python
sum1 = add_two_numbers(2, 4)
```

Dans la première ligne, on passe les **arguments** `2` et `4` **aux paramètres** `a` et `b`.

**NB** : il ne faut pas confondre **paramètre** et **argument**. Un paramètre est le nom que l'on donne à la variable d'entrée lors de la définition de la fonction (ici, `a` et `b` sont les paramètres de la fonction `add_two_numbers`). Les arguments sont les objets qui sont passés aux arguments de la fonction lors de l'appel de celle-ci (ici, l'argument `4` est passé au paramètre `a`, et l'argument `8` au paramètre `b`).

La console affiche `6`. L'objet renvoyé est bien référencé par la variable `sum1`.

### Définir un argument par défaut

Il est possible de définir un argument par défaut pour un paramètre. Exemple :

```python
def power(x, y=2):
    return x**y
```

Testons cette fonction :

```python
p1 = power(2)
p2 = power(4, 4)
```

La première ligne passe simplement l'argument `2` au paramètre `x`. Lors de l'exécution de la fonction, `y` prendra donc sa valeur par défaut, à savoir `2`.

La seconde ligne passe l'argument `2` au paramètre `x` et l'argument `4` au paramètre `y`. La valeur par défaut de `y` (`2`) est écrasée.

Si l'on ne connaît pas l'ordre des paramètres d'une fonction, il est possible de spécifier explicitement vers quel paramètre on passe tel ou tel argument :

```python
p3 = power(y=3, x=4)
```

### Le *type hint* d'un paramètre

Lors de la définition d'une fonction, il est également possible d'indiquer quel type on attend de recevoir pour un argument :

```python
def count_number_of_a(message: str):
    count = 0
    for char in message:
        if char == "a":
            count += 1
    return count
```

Testons cette fonction :

```python
nb_of_a = count_number_of_a("I have a certain quantity of a")
```

### Les variables locales

Cet exemple va nous permettre d'aborder le concept de variable locale. La variable `count` déclarée dans le bloc de la fonction `count_a(message)` est une variable locale. Cela signifie qu'elle "n'existe" que lors de l'appel de cette fonction, et que lorsque l'intérpréteur a terminé d'exécuter le code de celle-ci, la variable est "supprimée".

À l'inverse, une variable déclarée dans le contexte (*scope* en anglais) global est appelée variable globale. Si vous déclarez une variable avant la définition de vos fonctions, celle-ci sera accessible dans toutes vos fonctions.

**ATTENTION : Il est très peu recommandé d'utiliser des variables globales.** Nous verrons plus tard comment éviter d'en avoir dans nos scripts.

### Le mot-clef `return`

Il est possible d'écrire plusieurs `return` dans une fonction. À partir du moment où l'intérpréteur "arrive" sunr une instruction de renvoi, l'exécution de la fonction se termine en renvoyant ce qui est placé après le `return`, même si la fonction contient d'autres instructions en-dessous. On peut d'ailleurs utiliser un `return` sans rien renvoyer explicitement. Exemple :

```python
def guess_a_number():
    print("You have 10 tries to guess the secret number")
    tries = 10
    guess = int(input("input your first guess : "))
    while tries > 0:
        if guess == 13:
            print("Well played, you guessed the number !")
            return
        else:
            tries -= 1
            if tries == 0:
                print("You lost")
                return
            else:
                guess = int(input("Only " + str(tries) + " tries left. Enter a new guess : "))
```

Les fonctions présentent un intérêt fondamental : éviter d'avoir plusieurs fois le même bout de code dans son programme. En informatique, on cherche toujours à "compartimenter" son code afin d'éviter les répétitions inutiles.

## L'instruction `pass`

L'instruction `pass` est une instruction qui ne fait rien. Si elle ne fait rien, à quoi peut-elle servir ? Exemple.

Vous travaillez sur un script, et vous savez déjà que vous allez devoir définir une fonction spécifique, qu'on nommera, pour l'exemple, `sort_vegetables()`.

Pour l'instant, vous souhaitez définir cette fonction dans votre script, afin de "réserver" son nom, mais vous souhaitez écrire son contenu plus tard. Soit dans le code :

```python
def sort_vegetables():
    
```

Problème : si vous exécutez votre script en l'état, une erreur sera levée (et si vous utilisez Pycharm, l'erreur est détecté par l'éditeur). En effet, un bloc de code Python ne peut pas être vide. Pour pallier à ce problème, il suffit d'insérer l'instruction `pass` dans la fonction :

```python
def sort_vegetables():
    pass
```

Votre script pourra cette fois être exécuté sans problème.

## Les constantes

Une variable constante, est, comme on peut le deviner, une variable dont on ne peut pas changer la valeur après initialisation. Par exemple, en C++, on si l'on souhaite définir un entier constant, on écrit `const int myConstant;`.

Si le programme essaie d'affecter une nouvelle valeur à `myConstant`, cela engendrera une erreur.

En Python, on ne peux pas strictement définir une variable comme étant constante. En effet, il n'y a aucun mécanisme ou mot reservé permettant d'empécher la ré-affectation d'une valeur à une variable.

Par conséquent, si l'on souhaite définir une variable devant "faire semblant" d'être une constante, il faut lui donner un nom en majuscules . Par exemple, `DAYS_OF_WEEK` ou `ALPHABET_LOWERCASE`. Il faut bien comprendre ici que ce n'est qu'une convention qui permet d'indiquer aux autres que cette variable est une constante, et donc qu'il ne faut pas la modifier dans son propre code source.

## Exercices sur exercism.org :

- Hello World
- Guido's Gorgeous Lasagna
- Ghost Gobble Arcade grame
- Electric Bill
- Currency Exchange
- Leap
- Two Fer
- Square Root

**NB** : Ci-dessous, une solution de la *Task 6* de l'exercice *Currency Exchange* :

```python
def exchangeable_value(budget, exchange_rate, spread, denomination):
    """

    :param budget: float - the amount of your money you are planning to exchange.
    :param exchange_rate: float - the unit value of the foreign currency.
    :param spread: int - percentage that is taken as an exchange fee.
    :param denomination: int - the value of a single bill.
    :return: int - maximum value you can get.
    """
    new_rate = exchange_rate + (exchange_rate * (spread / 100))
    total_new_currency = exchange_money(budget, new_rate)
    bill_value_new_currency = int(total_new_currency / denomination)
    maximum_value_new_currency = bill_value_new_currency * denomination
    return maximum_value_new_currency
```
