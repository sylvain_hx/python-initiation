# print function, to show one text line in the console
print("hello world")

print(5+10)     # + : mathematics operator
x = 5           # x is a variable referencing to the int type object with a value of 5
y = 10
z = x + y       # z refers to the int object with a value of 15
print(z)

str1 = "hello world "       # String object : characters sequence
str2 = 'hello "friends" '    # String with ''
str3 = """I hope you are doing "well"\n me, yes"""    # String with triple "
print(str2 + str3)  # Concatenate two strings

first_char = str1[0]      # Get the first character of the string : index 0
third_char = str1[2]    # Get the 3rd character
last_char = str1[-1]     # Negative index : allow string browsing in reverse order
print(first_char)
print(third_char)
print(last_char)

nb = 5
nb_str = str(nb)    # Convert int nb into str
txt1 = "4"
txt1_int = int(txt1)       # Convert txt1 into int type number
txt2 = "5.23"
txt2_float = float(txt2)     # Convert txt2 into float type number

print(nb + txt1_int)
print(type(nb))
print(type(txt1))
print(type(txt2_float))

english_mark = 18
math_mark = 17.5
# "Old" method for string formatting
print("You got " + str(english_mark)
      + " in english and " + str(math_mark) + " in maths.")
# New, better method
print(f"You got {english_mark} in english and {math_mark} in maths.")

a = 5 < 6  # This comparison returns a boolean
print(a)

b = 5 < 6
c = 10 < 6

a = c and b    # and : logic operator
print(a)

nom = input("Enter your name : ")
print("Hello " + nom)
