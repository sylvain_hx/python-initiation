# Séance 14

## Exercice : consommation électrique mondiale

Source : https://www.kaggle.com/datasets/akhiljethwa/global-electricity-statistics

1. Calculez le total de la production nette en Afrique (de 1980 à 2021)
2. Calculez l'import net total de la Belgique (de 1990 à 2021)
3. Déterminez quel continent a la plus grande *installed capacity* en 2021
4. Déterminez la perte moyenne de distribution mondiale sur la décennie 2000-2009

<details>
  <summary markdown="span">Correction</summary>

```python
import pandas as pd
import numpy as np


def main():
    df = pd.read_csv("Global Electricity Statistics.csv")
    df = df.replace(["--", "N/A", "ie", 0], np.nan)  # Replaces every incorrect data with a NaN object
    df["Country"] = df["Country"].apply(lambda country_name: country_name.strip())  # Removes whitespace from names
    df["Country"] = df["Country"].str.strip()  # More concise than line above, same result. See exercise 1, question 4
    print(df)

    # Question 1
    df1 = df[(df["Region"] == "Africa") & (df["Features"] == "net generation")]
    print(df1)
    african_net_generation = df1[[str(year) for year in range(1980, 2022)]].astype(float).sum().sum()
    # First sum returns net generation per year (as a series), second calculates the sum of this series
    print(african_net_generation)

    # Question 2
    # TODO
    # Question 3
    # TODO
    # Question 4
    # TODO


if __name__ == "__main__":
    main()
```

</details>
