class Trouser:
    number_of_pants = 0     # class attribute

    def __init__(self, color, material, size, unit):
        self.color = color
        self.material = material
        if unit == "in":
            self.size = size
        elif unit == "cm":
            self.size = Trouser.cm_to_inches(size)
        # type(self).number_of_pants += 1
        Trouser.number_of_pants += 1    # Same as above

    def __str__(self):
        return f"The trouser is made of {self.material}, is {self.color} and measures {self.size}"

    def dye(self, new_color):
        self.color = new_color

    @classmethod
    def print_nb_of_pants(cls):
        print(f"{cls.number_of_pants} pants have been created as of now.")

    @classmethod
    def bluejeans(cls):
        return cls('blue', 'denim', 32, "in")

    @staticmethod
    def cm_to_inches(cm):
        return cm*0.3937008


def main():
    t = Trouser(0xFFFFFF, "Denim", 40, "cm")
    t2 = Trouser(0, "Cotton", 45, "cm")
    Trouser.print_nb_of_pants()
    bj = Trouser.bluejeans()
    bj = Trouser("blue", "denim", 32, "in")
    print(bj)
    bj.dye(0x123456)
    Trouser.print_nb_of_pants()
    print(Trouser.cm_to_inches(12))


if __name__ == "__main__":
    main()
