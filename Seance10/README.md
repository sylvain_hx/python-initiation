# Séance 10

## Les décorateurs

En Python, les fonctions sont, comme tout le reste, des objets. On peut donc affecter une fonction à une variable, comme on le ferait avec n'importe quel autre type.

```python
def yell(sentence):
    return sentence.upper() + " !"


meow = yell
print(meow('meow'))
```

La variable `meow` référence maintenant vers la fonction définie au-dessus. Il est donc possible d'exécuter le code de la fonction via `yell` et `meow`.

Le fait de pouvoir avoir une variable référençant vers une fonction permet de pouvoir passer une fonction en argument d'une autre fonction.

```python
def greet(func):
    greeting = func("Hello, I'm a student")
    print(greeting)

greet(yell)
```

Si on exécute ce code, on obtient en sortie :

```
HELLO, I'M A STUDENT !
```

Car dans la fonction `greet()`, le paramètre `func` reçoit en argument la fonction `yell`. On peut ensuite appeler cette fonction à l'intérieur de `greet()`

```python
def whisper(sentence):
    return sentence.lower() + "."

greet(whisper)
```

De la même manière, le code ci-dessus donnera en sortie :

```
hello, i'm a student.
```

Il est également possible de définir des fonctions dans des fonctions, et de retourner des fonctions :

```python
def get_volume_func(volume):
    def whisper(sentence):
        return sentence.lower() + "."
    
    def yell(sentence):
        return sentence.upper() + " !"
    if volume > 0.5:
        return yell
    else:
        return whisper

chosen_volume_function = get_volume_func(0.3)
print(chosen_volume_function("How are you"))
```

Ici, `chosen_volume` va donc faire référence vers la fonction `whisper`.

On peut réécrire une fonction qui aura le même comportement :

```python
def get_volume_func_bis(sentence, volume):
    def whisper():
        return sentence.lower() + "."
    
    def yell():
        return sentence.upper() + " !"
    if volume > 0.5:
        return yell
    else:
        return whisper
```

L'intérêt de cette syntaxe, c'est que les fonctions `whisper` et `yell` peuvent accéder au paramètre `sentence` de la fonction parent.

En pratique, ce principe d'imbriquer des fonctions ne permet pas seulement de renvoyer des fonctions par des fonctions, mais surtout de préconfigurer le comportement des fonctions. Autre exemple :

```python
def create_adder(a):
    def add(b):
        return a + b
    return add

x_plus_3 = create_adder(3)
print(x_plus_3(6))
```

La console affichera `9`, car `x_plus_3` fait référence vers une fonction `create_adder()` dans laquelle le paramètre `a` vaut `3`.

C'est sur ce principe que repose les décorateurs. Par exemple :

```python
def uppercase(func):
    def wrapper():
        original_output = func()
        modified_output = original_output.upper()
        return modified_output
    return wrapper

@uppercase
def say_goodbye():
    return "goodbye"

print(say_goodbye())
```

Le décorateur `@uppercase` placé au-dessus de la fonction `say_goodbye()` vient ainsi modifier son comportement. La console va en effet afficher

```
GOODBYE
```

Il est évidemment possible d'écrire des décorateurs et donc des fonctions *wrapper* qui prennent des arguments. Il faut pour cela utiliser les paramètres `*args` et `**kwargs`.

Ce qu'il est important de savoir, c'est qu'il existe des décorateurs permettant de modifier "pour vous" vos fonctions.

## Les membres de classe et statique

Jusqu'à présent, nous avons dans nos classes uniquement travaillées avec des membres (attributs et méthodes) d'instance. Mais il existe aussi des méthodes et attributs de classe et des méthodes statiques. Exemple :

```python
class Trouser:

    nb_of_pants = 0     # class attribute

    def __init__(self, color, material, size, unit):
        self.color = color
        self.material = material
        if unit == "in":
            self.size = size
        elif unit == "cm":
            self.size = Trouser.cm_to_inches(size)
        Trouser.nb_of_pants += 1

    def dye(self, new_color):
        self.color = new_color

    @classmethod
    def print_nb_of_pants(cls):
        print(f"{cls.nb_of_pants} pants have been created as of now.")

    @classmethod
    def bluejean(cls):
        return cls('blue', 'denim', 32, "in")

    @staticmethod
    def cm_to_inches(cm):
        return cm*0.3937008
```

`nb_of_pants` est un attribut de classe. C'est un attribut qui n'existe qu'une fois pour toute la classe, là où un attribut d'instance existe pour chaque instance (chaque objet créé à partir de la classe).

On peut accéder à un attribut de classe dans une méthode d'instance (ici `__init__(self)` en est une) via le nom de la classe elle-même.

Pour définir une méthode comme étant une méthode de classe, on utilise le décorateur `@classmethod`. Une méthode de classe n'a pas (par convention) `self` mais `cls` comme paramètre d'entrée, pour bien indiquer que la méthode va agir sur la classe et pas sur une instance de cette classe.

Ainsi, on voit dans `print_nb_of_pants` qu'on peut directement accéder à l'attribut de classe en écrivant `cls.nb_of_pants`.

Comme une méthode de classe ne travaille pas sur une instance, elle ne peut pas accéder à des attributs d'instances.

En pratique, on utilise souvent des méthodes de classe pour créer des *factory functions*. Par exemple, ici, la méthode de classe `bluejean` permet de créer automatique un jean bleu.

La dernière méthode, décoré par `@staticmethod`, est une méthode statique. C'est une méthode qui ne peut agir ni sur les instances, ni sur la classe (pas de paramètre `self` ou `cls`). Par conséquent, écrire des méthodes statiques révèle plus de la manière dont on souhaite compartimenter son code.

S'il y a une action particulière (ici convertir des centimètres en pouces) que vous devez souvent faire quand vous travailler avec des instances d'une classe, cela peut être judicieux d'écrire cette fonction en tant que méthode statique pour en quelque sorte "l'avoir sous la main".

Là encore, ce n'est pas forcément des concepts qu'on retrouve tout le temps en Python, mais qui sont quasiment systématiquement utilisés dans d'autres langages orientés objet.

Comment appeler ces différentes méthodes :

```python
if __name__ == "__main__":
    t = Trouser(0xFFFFFF, "Denim", 40, "cm")
    t2 = Trouser(0, "Cotton", 45, "cm")
    Trouser.print_nb_of_pants()
    bj = Trouser.bluejeans()
    bj.dye(0x123456)
    Trouser.print_nb_of_pants()
    print(Trouser.cm_to_inches(12))
```

Pour appeler une méthode de classe ou statique, on écrit `className.method()`. En effet, cela n'a pas vraiment de sens d'appeler ces méthodes via une instance de la classe.


## Exercices

- D&D Character
- Allergies
- Kindergarten Garden
- Robot Name