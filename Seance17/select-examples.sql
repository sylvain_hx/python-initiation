SELECT * FROM customers;

select * from db_test.articles;

select distinct country from classicmodels.customers;

select distinct country, city FROM classicmodels.customers;

select * from classicmodels.customers where creditLimit < 50000; 

select * from classicmodels.customers where country <> 'France';

select * from classicmodels.customers  order by country;

select * from classicmodels.payments order by amount desc;

select * from classicmodels.customers c order by country desc, creditLimit;

select * from classicmodels.products where buyPrice > 50 and quantityInStock > 1000;