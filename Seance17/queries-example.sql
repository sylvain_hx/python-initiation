
create database if not exists isib;

create table if not exists isib.students (
    PersonID int primary key,
    LastName varchar(255),
    FirstName varchar(255)
);

insert into isib.students (PersonID, FirstName, LastName)
values (54014, 'Sylvain', 'Huraux');

insert into isib.students (PersonID)
values (60241);

insert into isib.students values
(47222, 'Doe', 'John'),
(49532, 'Smith', 'Joe'),
(63254, 'Keller', 'Alice');

select * from isib.students where LastName is not null;

update isib.students set FirstName='Sylvain', LastName='Huraux' where PersonID=54014;


DELETE FROM isib.students WHERE LastName='Miller';

create table isib.teachers 
(TeacherID int);

insert into isib.teachers values (3)

delete from isib.teachers;	# supprime toutes les lignes de la table

drop table isib.teachers;	# supprime LA table

select * from classicmodels.customers where country='USA' limit 5;

select * from classicmodels.customers where country='France' order by contactLastName limit 10;
