select min(buyPrice) from classicmodels.products;
select max(buyPrice) from classicmodels.products;

SELECT COUNT(productCode)
FROM classicmodels.products
WHERE buyPrice > 50;

select sum(creditLimit) from classicmodels.customers;

SELECT SUM(priceEach * quantityOrdered) as totalPrice
FROM classicmodels.orderdetails;

select avg(buyPrice) as averagePrice from classicmodels.products;

select * from classicmodels.customers 
where contactLastName like 'E%' or contactLastName like '%e' or contactLastName like '%e%';

create database if not exists test;
